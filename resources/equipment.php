<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Equipment | Available Resources | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Available Equipment">
  <meta name="og:description" content="The Advanced Visualization Center has a large library of visualization equipment that students and staff can use and check out.">
  <meta name="description" content="Information for the USF AVC's equipment checkout.">
  <meta name="og:image" content="media/3D_printing/printed_spike_makerbot.jpg">

  <meta name="og:image:alt" content="One of the AVC's 3D printers prints a translucent model of a droplet of liquid making waves.">
  <meta name="og:url" content="<?php echo $root ?>/resources/equipment.php">

  <?php
    $PHPTHUMB_CONFIG['cache_source_filemtime_ignore_remote'] = true;
    $sheetname = "Inventory!A1:I1000";

    $cols = array(
      'name' => 1,
      'desc' => 2,
      'cat'  => 3,
      'stat' => 6,
      'url'  => 7,
      'img'  => 8
    );

    $options = array(
      'ssl'=>array(
        'cafile' => '../cacert.pem',
        'verify_peer' => true,
        'verify_peer_name' => true,
      ),
    );
    $context = stream_context_create( $options );
    $url = "https://sheets.googleapis.com/v4/spreadsheets/1d7rJTeY98GoIY-AVhb4AimjrpPC3WxHmKzypo4LpOdg/values/" . $sheetname . "?key=AIzaSyCYlI6lXCMHo8tjGgRqSXrbOiApB4Rg4LA";
    $data = file_get_contents( $url, FILE_TEXT, $context );

    $parsed_data = json_decode($data)["values"];
    $categories = array();

    foreach((array)$parsed_data as $rowindex => $row) {

      if($rowindex != 0 && isset($row[$cols['name']])) { // Skip title row and empty rows
        $fields = array(
          'name' => htmlspecialchars($row[$cols['name']]),
          'desc' => isset($row[$cols['desc']]) ? htmlspecialchars($row[$cols['desc']]) : '',
          'cat'  => isset($row[$cols['cat']] ) ? htmlspecialchars($row[$cols['cat']])  : '',
          'stat' => isset($row[$cols['stat']]) ? htmlspecialchars($row[$cols['stat']]) : '',
          'url'  => isset($row[$cols['url']] ) ? htmlspecialchars($row[$cols['url']])  : '',
          'img'  => isset($row[$cols['img']] ) ? htmlspecialchars($row[$cols['img']])  : '../../resources/template/placeholder.png'
        );

        if (!isset($categories[$fields['cat']])) {
          $categories[$fields['cat']] = array();
        }

        $phpThumbUrlLowRes = htmlspecialchars(phpThumbURL('src=' . $fields['img'] . '&w=150&h=150&q=30&far=C&bg=FFFFFF', 'libraries/phpThumb/phpThumb.php'));
        $phpThumbUrlHighRes = htmlspecialchars(phpThumbURL('src=' . $fields['img'] . '&w=300&h=300&q=95&far=C&bg=FFFFFF', 'libraries/phpThumb/phpThumb.php'));

        $status = 'hidden'; // Default
        switch($fields['stat']) {
          case 'Checked Out':
            $status = 'not-available';
            $statusText = 'Not Available';
            break;
          case 'Available for Use':
            $status = 'available-use';
            $statusText = '<abbr title="Available">Avbl.</abbr> to Use';
            break;
          case 'Available for Checkout';
            $status = 'available-checkout';
            $statusText = '<abbr title="Available">Avbl.</abbr> to Borrow';
            break;
        }

        $link = isset($row[$cols['url']] ) ? '<a class="more-info arrow" href="' . $fields['url'] . '">Detailed <abbr title="Information">Info</abbr></a>' : '';

        if($status != "hidden") {
          $html = '<figure class="grid-item ' . $status . '" itemprop="itemOffered" itemscope itemtype="http://schema.org/IndividualProduct">
              <img itemprop="image" class="responsive-resolution" src="' . $phpThumbUrlLowRes . '" alt="" data-hd-src="' . $phpThumbUrlHighRes . '">
              <figcaption>
                <h4 title="' . $fields['name'] . '" itemprop="name">' . $fields['name'] . '</h4>
                <p class="short-description" itemprop="category">' . $fields['desc'] . '</p>
                <small class="status" itemprop="itemcondition">' . $statusText . '</small>
                ' . $link . '
              </figcaption>
            </figure>';

          // Make sure that unavailable items are pushed to the end of the section
          if($status == 'not-available') {
            array_push($categories[$fields['cat']], $html);
          } else {
            array_unshift($categories[$fields['cat']], $html);
          }
        }
      }
    };
  ?>
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <article itemprop="makesOffer" itemscope itemtype="http://schema.org/Offer">
        <h2 id='Equipment' itemprop="name">Equipment</h2>
        <p itemprop="description">USF students and faculty may request to use the following equipment by visiting <a href="about/locations.php#main-content" title="Office map and hours">our office in CMC 147</a>. For more information about available items, please <a href="about/">contact us</a>.</p>
        
        <section class="table-of-contents">
          <h3 class="accent-header"><span>Contents</span></h3>
          <ul class="bullet-list">
            <?php

              foreach($categories as $i => $category) {
                echo('<li><a href="resources/equipment.php#' . urlencode($i) . '">' . $i . '</a></li>');
              }

            ?>
          </ul>
        </section>
        
        <?php

          foreach($categories as $i => $category) {
            echo('<section>
              <h3 id="'. urlencode($i) . '">' . $i . '</h3>
                <div class="gallery-info-grid">');
            foreach($category as $j => $item) {
              echo($item);
            }
            echo('</div>
            </section>');
          }

        ?>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>
</body>
</html>
