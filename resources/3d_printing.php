<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>3D Printing | Available Resources | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - 3D Printing Services">
  <meta name="og:description" content="Information and prices for the USF Advanced Visualization Center's 3D printing services, offered at the 3D Printing Labs on campus.">
  <meta name="description" content="Information and prices for the USF AVC's 3D printing services.">
  <meta name="og:image" content="media/3D_printing/resin_bug.jpg">

  <meta name="og:image:alt" content="Small transparent insect printed from liquid resin at the USF AVC.">
  <meta name="og:url" content="<?php echo $root ?>/resources/3d_printing.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
      <a class="skip-to-main" href="//avc.web.usf.edu/resources/3d_printing.php#main-content">Skip to main content</a>
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width" itemprop="makesOffer" itemscope itemtype="http://schema.org/Offer">
      <?php include "template/menu.php";?>
      <article class="no-justify" itemprop="itemOffered" itemscope itemtype="http://schema.org/Service">
        <h2 itemprop="name">3D Printing</h2>
        <p itemprop="description">The AVC offers 3D printing services at <a href="about/locations.php#main-content">our 3D Printing Labs</a> on campus. To submit files for printing, email them to <a href="mailto:advancedvisualizationcenter@gmail.com">AdvancedVisualizationCenter@gmail.com</a>. If you have any questions, please refer to our <a href="about/index.php">contact page</a>.
        
        <section class="table-of-contents">
          <h3 class="accent-header"><span>Contents</span></h3>
          <ul class="bullet-list">
            <li><a href="resources/3d_printing.php#prices">Printing Prices</a></li>
            <li><a href="resources/3d_printing.php#policies">Policies/Notes</a></li>
            <li><a href="resources/3d_printing.php#scanning">3D Scanning</a></li>
          </ul>
        </section>

        <section class="image-row" aria-label="Image Gallery">
          <div class="image-row-container" data-featherlight-gallery data-featherlight-filter="a">
            <div>
              <a href="media/3D_printing/3d_printers.jpg">
                <?php echo '<img itemprop="image" alt="A small sample of the 3D Printers available in our printing lab." src="'.htmlspecialchars(phpThumbURL('src=../../media/3D_printing/3d_printers.jpg&w=200&h=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'">';?>
              </a>
            </div>
            <div>
              <a href="media/3D_printing/printed_dinosaur.jpg">
                <?php echo '<img alt="High-quality small blue 3D printed dinosaur head, highlighting detailed printing capabilities." src="'.htmlspecialchars(phpThumbURL('src=../../media/3D_printing/printed_dinosaur.jpg&w=200&h=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'">';?>
              </a>
            </div>
            <div>
              <a href="media/3D_printing/printed_heart.jpg">
                <?php echo '<img alt="Two white printed anatomical hearts, showing scaling capabilites." src="'.htmlspecialchars(phpThumbURL('src=../../media/3D_printing/printed_heart.jpg&w=200&h=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'">';?>
              </a>
            </div>
            <div>
              <a href="media/3D_printing/printed_people.jpg">
                <?php echo '<img alt="Several miniature printed busts, showing 3D scanning and detailed printing." src="'.htmlspecialchars(phpThumbURL('src=../../media/3D_printing/printed_people.jpg&w=200&h=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'">';?>
              </a>
            </div>
            <div>
              <a href="media/3D_printing/resin_bug.jpg">
                <?php echo '<img alt="A small transparent bug, printed with a resin process that makes it perfectly smooth." src="'.htmlspecialchars(phpThumbURL('src=../../media/3D_printing/resin_bug.jpg&w=200&h=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'">';?>
              </a>
            </div>
            <div>
              <a href="media/3D_printing/printed_spike_makerbot.jpg">
                <?php echo '<img alt="One of our Makerbot Replicator 2 printers prints a translucent model of a droplet of liquid making waves." src="'.htmlspecialchars(phpThumbURL('src=../../media/3D_printing/printed_spike_makerbot.jpg&w=200&h=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'">';?>
              </a>
            </div>
          </div>
        </section>

        <section itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
          <h3 id="prices">Printing Prices</h3>
          <p>Estimated printing prices are as follows. For an exact quote, please <a href="about/locations.php#main-content">visit us in person</a>. Printing is charged with <a href="https://usfweb3.usf.edu/bullbucks/" itemprop="acceptedPaymentMethod" itemscope itemtype="http://schema.org/PaymentMethod"><span itemprop="name">USF Bull Buck$</span></a>, with a minimum charge of <span itemprop="priceCurrency" content="USD">$</span><span itemprop="lowPrice">1.00</span>.</p>
            
          <table class="clean-table">
            <caption>Estimated 3D Printing Prices</caption>
            <thead>
              <tr>
                <th scope="col" id="printPrep">Print & Prep Time</th>
                <th scope="col" id="basePrice">Base Price</th>
                <th scope="col" id="timeSurcharge">Time Surcharge</th>
              </tr>
            </thead>

            <tbody>
              <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <th scope="row" itemprop="name" headers="printPrep" id="zero-five">0-5 hours</th>
                <td itemprop="priceSpecification" headers="basePrice zero-five" itemscope itemtype="http://schema.org/UnitPriceSpecification">
                  <span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">0.06</span> / <span itemprop="unitCode" content="GRM">gram</span>
                </td>
                <td headers="timeSurcharge zero-five">$0.00</td>
              </tr>
              <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <th scope="row" itemprop="name" headers="printPrep" id="five-eight">5-8 hours</th>
                <td itemprop="priceSpecification" headers="basePrice five-eight" itemscope itemtype="http://schema.org/UnitPriceSpecification">
                  <span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">0.06</span> / <span itemprop="unitCode" content="GRM">gram</span>
                </td>
                <td headers="timeSurcharge five-eight"><span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">1.00</span></td>
              </tr>
              <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <th scope="row" itemprop="name" headers="printPrep" id="eight-eleven">8-11 hours</th>
                <td itemprop="priceSpecification" headers="basePrice eight-eleven" itemscope itemtype="http://schema.org/UnitPriceSpecification">
                  <span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">0.06</span> / <span itemprop="unitCode" content="GRM">gram</span>
                </td>
                <td headers="timeSurcharge eight-eleven"><span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">3.00</span></td>
              </tr>
              <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <th scope="row" itemprop="name" headers="printPrep" id="eleven-fourteen">11-14 hours</th>
                <td itemprop="priceSpecification" headers="basePrice eleven-fourteen" itemscope itemtype="http://schema.org/UnitPriceSpecification">
                  <span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">0.06</span> / <span itemprop="unitCode" content="GRM">gram</span>
                </td>
                <td headers="timeSurcharge eleven-fourteen">
                  <span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">5.00</span> + 
                  <span itemprop="priceSpecification" itemscope itemtype="http://schema.org/UnitPriceSpecification">
                    <span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">2.00</span> / <span itemprop="billingIncrement">3</span> <span itemprop="unitCode" content="HUR">hours</span>
                  </span>
                </td>
              </tr>
              <tr itemprop="offers" itemscope itemtype="http://schema.org/Offer" itemref="schema-altoffer-description">
                <th scope="row" id="bulk"><span headers="printPrep" itemprop="name">Bulk Purchase Option</span> *</th>
                <td headers="basePrice bulk"><span itemprop="priceCurrency" content="USD">$</span><span itemprop="price">60.00</span></td>
                <td headers="timeSurcharge bulk">$0.00</td>
              </tr>
            </tbody>

            <tfoot>
              <tr>
                <td colspan="3" itemscope id="schema-altoffer-description"><span itemprop="description" class="footnote" data-footnote-symbol="*">As an alternative to the base pricing, 1kg of filament can be purchased in advance for $60.00. With this option, no time surcharge will be applied.</span></td>
              </tr>
            </tfoot>
          </table>

        </section>

        <section>
          <h3 id="policies">Policies / Notes</h3>
          <p>Please read the following notes carefully before using the 3D printing services:</p>
          <ul class="bullet-list">
            <li>Daily 2D free printing allowance does not apply to 3D printing.</li>
            <li>Is is your responsibility to ensure that your files are print-ready.</li>
            <li>Files may be emailed or brought to the lab.</li>
            <li>You will be notified of estimated print times and costs prior to printing.</li>
            <li>Please be sure to request the appropriate material (PLA/ABS**) and/or color for your print.</li>
            <li>Reprints due to file errors will be charged accordingly.</li>
            <li>If you do not wish to do a reprint when there is a printer error you will not be charged for the print.</li>
            <li>Payments must be recieved befoer the print will be released.</li>
            <li>Prices vary per print and are subject to change.</li>
          </ul>
          <br>
          <p class="footnote" data-footnote-symbol="**">For other filaments or printing types (ie flexible filament or resin printing), <a href="about/">contact us</a>. Note that additional costs will apply and an advance meeting is required.</p>
        </section>

        <section>
          <h3 id="scanning">3D Scanning</h3>
          <p>The AVC also offers 3D scanning services upon request, allowing physical objects to be turned into 3D models (which can then be printed). See the <a href="resources/equipment.php#3D+Scanners">Equipment</a> page for a list of available scanners.</p>
        </section>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>

  <script src="libraries/cssvars.ponyfill.js"></script>
  <script src="scripts/global.js"></script>
  
  <script src="libraries/jquery-3.3.1.min.js"></script>
  <script src="libraries/featherlight/release/featherlight.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.min.css">
  <script src="libraries/featherlight/release/featherlight.gallery.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.gallery.min.css">
  <script src="scripts/featherlight_config.js"></script>
</body>
</html>
