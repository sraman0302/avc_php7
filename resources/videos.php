<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Video Titles | Available Resources | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Video Titles">
  <meta name="og:description" content="These videos are available for shown on the USF AVC Advanced Visualization Wall.">
  <meta name="description" content="These videos are available for shown on the USF AVC Advanced Visualization Wall.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="<?php echo $root ?>/resources/videos.php">

</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <article>
        <h2 id="Video+Titles">Video Titles</h2>
        <p>See the following file for available videos for showing on the AVC Visualization Wall:</p>

        <div class="pdf-viewer">
          <object data="resources/media/video_titles.pdf" type="application/pdf">
            <a href="resources/media/video_titles.pdf" class="arrow">Video Titles.pdf</a>
          </object>
        </div>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>
</body>
</html>
