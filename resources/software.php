<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Software | Available Resources | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Software">
  <meta name="og:description" content="This software is available for use at the AVC Computer Lab or on the AVC Visualization Wall.">
  <meta name="description" content="This software is available for use at the AVC Computer Lab or on the AVC Visualization Wall.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="<?php echo $root ?>/resources/software.php">

</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <article>
        <h2 id="Software">Software</h2>
        <p>The following software is available for use at the AVC Computer Lab or on the AVC Visualization Wall. To request
          other software, please <a href="about/">contact us</a>. If you plan on using software in this list for a
          class or presentation, we still advise that you contact us ahead of time in order to ensure that the software is
          updated and configured as necessary.</p>
        <ul class="responsive-list with-sublists">
          <li>Autodesk
          <ul>
            <li><b>Inventor</b></li>
            <li><b>Mudbox</b></li>
            <li><b>Maya 2018</b></li>
          </ul></li>
          <li>Adobe
          <ul>
            <li><b>Photoshop</b></li>
            <li><b>Illustrator</b></li>
            <li><b>After Effects</b></li>
            <li><b>Premiere Pro</b></li>
            <li><b>Acrobat</b></li>
          </ul></li>
          <li><b>Unity 2018</b> with plugins for:
          <ul>
            <li>Windows</li>
            <li>iOS</li>
            <li>Android</li>
            <li>WebGL</li>
            <li>Vuforia</li>
            <li>OpenVR</li>
            <li>Oculus</li>
          </ul></li>
          <li><b>Java</b></li>
          <li><b>Gephi</b></li>
          <li><b>Matlab</b></li>
          <li><b>Anaconda</b> with plugins for:
          <ul>
            <li>Spyder</li>
            <li>Orange3</li>
            <li>RStudio</li>
          </ul></li>
          <li><b>X2Go</b></li>
          <li><b>Putty</b></li>
          <li><b>Python3</b></li>
          <li><b>Visual Studio Code</b> with plugins for:
          <ul>
            <li>Python</li>
            <li>JavaScript</li>
            <li>HTML</li>
            <li>CSS</li>
            <li>C/C++</li>
            <li>Git</li>
          </ul></li>
          <li><b>Sublime Text</b></li>
          <li><b>Notepad++</b></li>
          <li>Valve
          <ul>
            <li><b>Steam</b></li>
            <li><b>SteamVR</b></li>
          </ul></li>
          <li><b>Oculus</b></li>
          <li><b>Google Chrome</b></li>
          <li><b>Mozilla Firefox</b></li>
          <li><b>MinGW</b>
          <ul>
            <li>gcc</li>
            <li>g++</li>
            <li>msys</li>
          </ul></li>
          <li><b>7-Zip</b></li>
          <li>Microsoft Office
          <ul>
            <li><b>Excel</b></li>
            <li><b>Word</b></li>
            <li><b>Powerpoint</b></li>
          </ul></li>
          <li><b>Node.JS</b></li>
          <li><b>Meshmixer</b></li>
          <li><b>Meshlab</b></li>
          <li><b>Solidworks</b></li>
          <li><b>FileZilla</b></li>
          <li><b>Audacity</b></li>
          <li><b>VLC Media Player</b></li>
          <li><b>Android Studio</b></li>
          <li><b>Git</b></li>
          <li><b>VirtualBox</b></li>
          <li><b>Tableau</b></li>
          <li><b>MySQL Workbench</b></li>
          <li><b>Google Earth Pro</b></li>
          <li><b>Processing</b></li>
          <li><b>ZBrush</b></li>
          <li><b>ArcGIS</b></li>
        </ul>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>
</body>
</html>
