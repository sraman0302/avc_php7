<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="theme-color" content="#10805e">
  <meta name="geo.region" content="US-FL">
  <meta name="geo.placename" content="Tampa">
  <meta name="og:locale" content="en_US">
  <meta name="og:site_name" content="USF Advanced Visualization Center">
  <meta name="twitter:card" content="summary_large_image">

    <base href='http://127.0.0.1:8080/edsa-AVC%201/'>

  
  
  <link rel="stylesheet" href='template/resets.css'>
  <link rel="stylesheet" href='template/template.css'>
  <link rel="stylesheet" href='template/widgets.css'>
  <link rel="shortcut icon" type="image/ico" href='template/favicon.ico'/>

  <title>Available Resources | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Available Resources">
  <meta name="og:description" content="The Advanced Visualization Center provides a number of resources to USF students, faculty, and external parties.">
  <meta name="description" content="Information about the USF AVC's available resources.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="https://avc.web.usf.edu//resources/">

</head>
<body itemscope itemtype="http://schema.org/Organization">
  <a class="skip-to-main" href="//avc.web.usf.edu/resources/index.php#main-content">Skip to main content</a> 

<header>
  <div class="max-width" id="global-header" itemprop="parentOrganization" itemscope itemtype="http://schema.org/EducationalOrganization">
    <a id="global-logo" href="http://usf.edu" itemprop="url"><span itemprop="name">University of South Florida</span></a>
    <nav>
      <a href="http://my.usf.edu">MyUSF</a>
      <a href="http://directory.usf.edu/">Directory</a>
      <a href="http://www.usf.edu/it/">Information Technology</a>
    </nav>
  </div>
  <div id="site-header">
    <div class="max-width">
      <h1>
        <a href="/" itemprop="url"><span itemprop="name">Advanced Visualization Center</span></a>
      </h1>
      <h2 itemprop="parentOrganization" itemscope itemtype="http://schema.org/Organization">
        A Division of <a href="http://www.usf.edu/it/research-computing/" itemprop="url"><span itemprop="name">Research Computing</span></a>
      </h2>
    </div>
  </div>
</header>
  <nav id="site-menu">
  <div class="max-width">
    <h2>Navigation</h2>
    <a href="/">Home</a>
    <a href="schedule.php">Schedule</a>
    <a href="resources/index.php">Resources</a>
    <a href="showcase/index.php">Showcase</a>
    <a href="resources/3d_printing.php">3D Printing</a>
    <a href="about/index.php">About Us</a>
  </div>
</nav>

  <main id="main-content">
    <header class="page-header max-width">
  <h1>Available Resources</h1>
  <p>The following resources are provided by the Advanced Visualization Center.</p>
</header>
    <div class="has-navigation dont-collapse-nav default-body max-width">
      <nav class="secondary-navigation">
  <h2>Navigation</h2>
  <a href="resources/">Overview</a>
  <a href="resources/equipment.php">Equipment</a>
  <a href="resources/rooms.php">Rooms</a>
  <a href="resources/software.php">Software</a>
  <a href="resources/videos.php">Video Titles</a>
  <a href="resources/3d_printing.php">3D Printing</a>
  <a href="resources/services.php">Services</a>
</nav>
      <article>
        <h2>Overview</h2>
        <p>The Advanced Visualization Center provides a number of services to USF students, faculty, researchers, and external parties. Services range from entry level introduction to technology integration for academics and research to full development of customized applications for course implementation, and research.</p>
      </article>
    </div>
  </main>

    <footer class="max-width">
    <div id="site-footer">
      <div id="site-info">
        <a href="http://usf.edu">
          <img src='template/footer-logo.png' alt="University of South Florida Logo">
        </a>
        <div>
          <address>
            <div>Copyright © 2018, University of South Florida Advanced Visualization Center. All rights reserved.</div>
            <div><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" id="schema-avc-address"><span itemprop="streetAddress">12010 USF Cherry Drive, CMC147</span>, <span itemprop="addressLocality">Tampa</span>, <span itemprop="addressRegion">FL</span> <span itemprop="postalCode">33620</span>, <span itemProp="addressCountry">USA</span></span></div>
            <div>This website is maintained by the <a href="about/">Advanced Visualization Center (AVC)</a>.</div>
          </address>
          <nav>
              <a href="about/">Contact AVC</a>
              <a href="http://www.usf.edu/about-usf/contact-usf.aspx" target="_blank">Contact USF</a>
              <a href="http://www.usf.edu/about-usf/visit-usf.aspx" target="_blank">Visit USF</a>
              <a href="https://github.com/usf-avc/public-website/issues" target="_blank">Report Site Issues</a>
          </nav>
        </div>    
      </div>
    </div>
  </footer>

  <script src='libraries/cssvars.ponyfill.js'></script>
  <script src='scripts/global.js'></script>
  </body>
</html>