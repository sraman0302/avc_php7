<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Services | Available Resources | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Services">
  <meta name="og:description" content="The Advanced Visualization Center provides assistance and guidance with the process of creating data visualizations, interactive applications, augmented and virtual reality, 3D modeling and printing for course related assignments, research projects and instructional aids.">
  <meta name="description" content="Information for the USF AVC's other services.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="<?php echo $root ?>/resources/services.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <article class="space-paragraphs">
        <h2>Services</h2>
        <section>
          <h3>Student and Faculty Services</h3>
          <p>The Advanced Visualization Center provides assistance and guidance with the use of advanced technologies and the process of creating interactive applications such as augmented & virtual reality and <a href="resources/3d_printing.php" title="AVC 3D Printing Lab information">3D modeling & printing</a> for course related assignments, research projects and instructional aids.</p>
          <p>Students or faculty needing assistance are encouraged to <a href="about/" title="AVC contact information">contact the Advanced Visualization Center</a>.</p>
        </section>
        <section>
          <h3>Grants</h3>
          <p>We are available to consult and develop applications for research grants. Typically, we are involved in the technology portion of the grant proposal process. For more information on how we can assist with grants, <a href="about/#hkaplan" title="Howard Kaplan contact information">contact Howard Kaplan, Advanced Technologies Manager</a></p>
        </section>
        <section>
          <h3>Private Services (External to the USF System)</h3>
          <p>External parties interested in the services provided by the Advanced Visualization Center should <a href="about/#hkaplan" title="Howard Kaplan contact information">contact Howard Kaplan</a> to determine if we can accommodate your project requirements.</p>
        </section>
        <section>
          <h3>Citing the AVC</h3>
          <p>When using the Advanced Visualization services students, faculty, and researchers should cite the AVC properly.</p>
          <p>The recognition of the AVC resources used to perform research is important for acquiring continued funding for the next generation of hardware, support services, and our research and development activities in advanced technology integration at USF.</p>
          <p>At minimum, a citation should include:</p>
          <small>Advanced Visualization Center (AVC), Research Computing (RC) at University of South Florida</small>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>
</body>
</html>
