<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>
     <base href="http://127.0.0.1:8080/edsa-AVC%201/">

  
  
  <link rel="stylesheet" href="template/resets.css">
  <link rel="stylesheet" href="template/template.css">
  <link rel="stylesheet" href="template/widgets.css">
  <link rel="shortcut icon" type="image/ico" href="template/favicon.ico"/>


  <title>Rooms | Available Resources | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Rooms">
  <meta name="og:description" content="The AVC is responsible for several lecture and presentation rooms that are equipped with advanced visualization technology.">
  <meta name="description" content="Information about the USF AVC's Advanced Visualization Auditorium and Visualization Computer Lab.">
  <meta name="og:image" content="resources/media/auditorium.jpg">

  <meta name="og:image:alt" content="View of the AVC Auditorium, with several rows of seating and Avanced Visualization Wall.">
  <meta name="og:url" content="<?php echo $root ?>/resources/rooms.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <section>
        <h2>Room Usage</h2>
        <p>The AVC is responsible for several lecture and presentation rooms that are equipped with advanced visualization technology. These rooms can be reserved according to the policies below.
        
        <section class="table-of-contents">
          <h3 class="accent-header"><span>Contents</span></h3>
          <ul class="bullet-list">
            <li><a href="resources/rooms.php#auditorium">Advanced Visualization Auditorium</a></li>
            <li><a href="resources/rooms.php#computerlab">XR Lab</a></li>
          </ul>
        </section>

        <article class="space-paragraphs">
          <h3 id="auditorium">Advanced Visualization Auditorium</h3>
            <a href="resources/media/auditorium.jpg" data-featherlight="image">
                <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../resources/media/auditorium.jpg&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="AVC Auditorium, with several rows of seating and Avanced Visualization Wall">';?>
            </a>
            <p>The Advanced Visualization Auditorium is a theater-like room that is home to the Advanced Visualization Wall, a high-resolution, 180" display wall consisting of 16 45" monitors. The display wall supports 3D visualization (using 3D glasses) and is equipped with a powerful Windows 7 computer and speaker system. The wall can also be used with personal laptops upon request. The Auditorium is located on campus in <a href="about/locations.php#main-content" title="Location map">CMC 147</a>.</p>
            <a class="primary-button" data-featherlight="iframe" href="https://docs.google.com/forms/d/e/1FAIpQLSd6QpY4WZz7JkPsg6GIZtC68Hqo2EHkduku1UazW4tcL7OVDA/viewform?embedded=true" target="_blank">Reservation Form</a>

          <div class="pano" id="pano-auditorium" data-src="resources/media/auditorium_360 (4096).jpg" data-type="equirectangular">
            <noscript><br><a class="arrow" href="resources/media/auditorium_360.jpg">View full panoramic image</a></noscript>
          </div>

          <h4 id="auditorium-scheduling">Scheduling Policy</h4>
            <p>The Advanced Visualization Auditorium has been established and equipped to serve as a teaching facility, as part of the <a href="http://www.usf.edu/it/tech-fee/index.aspx">Student Tech Fee</a>. As such it can be utilized by any USF academic department for class instruction or research purposes. Theses classes have priority over any other use of the space.</p>
            <p>Since this classroom provides advanced technology for computing and displaying content, faculty demonstrations must meet the needs of this technology for approved use. While scheduling the facility, through the AVC website, instructors will be asked to provide a brief description of their intended use.</p>
            <p>The Visualization Auditorium is scheduled on a first come, first served basis. Current approved room reservations are shown on the <a href="schedule.php">AVC Schedule</a>. When the reservation form has been filled out and submitted, it will be sent to the AVC Committee for review. Once the committee has approved the request the faculty member will be notified by email and the event will be added to the AVC online schedule.</p>
            <p>If the request has been denied the faculty member that submitted the request will be notified by email with an explanation of the review. The committee may also request more information if necessary.</p>
            <p>It is required that first time users of the Visualization Auditorium, meet with the AVC staff, at least 24 hours prior to the scheduled event. One of the AVC staff members will walk you through a quick training session and answer any questions you may have about the equipment, resources and data transfer process.</p>
          
          <h4>Software</h4>
            <p>A list of installed software is available <a href="resources/software.php">here</a>. Special requests for specific software or hardware must be made in advance by <a href="about/">contacting us</a> in advance. The installation of software by anyone not directly employed by the AVC is prohibited.</p>
          
          <h4>Templates</h4>
            <p>The following files are available for assistance with developing for the Visualization Wall:</p>
            <ul>
              <li><a href="resources/media/avc_vis_wall_template.mb">Maya Template</a></li>
              <li><a href="resources/media/avc_vis_wall_template.jpg">Image Template</a></li>
            </ul>
          
          <h4>Non-Instructional Use</h4>
            <p>The Visualization Auditorium is available for non-USF organizations at an hourly rate. Please <a href="about/">contact us</a> for more information.</p>
        </article>

        <article class="space-paragraphs">
          <h3 id="computerlab">XR Lab</h3>
            <a href="resources/media/computerlab.jpg" data-featherlight="image">
              <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../resources/media/computerlab.jpg&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="AVC Computer Lab, with a forensics class in progress">';?>
            </a>
            <p>The XR Lab contains a number of high-performance Windows computers, specialized for use in developing visualization projects. These computers are pre-equipped with visualization software and can be accessed with a valid <a href="http://www.usf.edu/it/documentation/netid.aspx">NetID</a>. The room also contains a speaker system and projector for presentation and teaching purposes.  The Computer Lab is located on campus in <a href="about/locations.php#main-content" title="Location map">SCA 222</a>.</p>
            <p>The XR Lab also contains equipment for developing and using Virtual, Augmented, and Mixed Reality. See our <a href="https://avc.web.usf.edu/resources/equipment.php#VR%2FAR+Headsets">Equipment page</a> for available devices.</p>
            <a class="primary-button" data-featherlight="iframe" href="https://docs.google.com/forms/d/e/1FAIpQLSd6QpY4WZz7JkPsg6GIZtC68Hqo2EHkduku1UazW4tcL7OVDA/viewform?embedded=true" target="_blank">Reservation Form</a>

          <h4>Usage Policy</h4>
            <p>Students have priority over the lab resources during open lab hours. The current open hours are available <a href="about/locations.php#main-content">here</a>. The lab is open to students on a first come first serve bases. Students are required to sign-in with the lab monitor when entering the facility.</p>
            <p>Utilization of the lab is granted for visualization projects and assignments for USF courses. Use of the open lab hours for other purposes is prohibited.</p>
          
            <h4>Scheduling Policy</h4>
            <p>The Visualization Computer Lab shares the same <a href="resources/rooms.php#auditorium-scheduling">scheduling policy</a> as the Advanced Visualization Auditorium. Please read in full before completing the reservation form.</p>
        </article>
      </section>
    </div>
  </main>

  <?php include "../template/footer.php"?>

  <script src="libraries/pannellum/pannellum.js"></script>
  <link rel="stylesheet" href="libraries/pannellum/pannellum.css">
  <script src="scripts/pannellum_config.js"></script>
  <script src="libraries/jquery-3.3.1.min.js"></script>
  <script src="../libraries/featherlight/release/featherlight.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.min.css">
  <script src="scripts/featherlight_config.js"></script>
</body>
</html>
