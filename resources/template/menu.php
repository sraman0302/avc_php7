<nav class="secondary-navigation">
  <h2>Navigation</h2>
  <a href="resources/">Overview</a>
  <a href="resources/equipment.php">Equipment</a>
  <a href="resources/rooms.php">Rooms</a>
  <a href="resources/software.php">Software</a>
  <a href="resources/videos.php">Video Titles</a>
  <a href="resources/3d_printing.php">3D Printing</a>
  <a href="resources/services.php">Services</a>
</nav>
