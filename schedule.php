<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "template/head.php";?>

  <title>Schedule | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Schedule">
  <meta name="og:description" content="This schedule shows all events related to the USF Advanced Visualization Center.">
  <meta name="description" content="This schedule shows all events related to the USF Advanced Visualization Center.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="<?php echo $root ?>/schedule.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "template/header.php";?>
  <?php include "template/menu.php";?>

  <main id="main-content">
    <header class="page-header max-width">
      <h1>Schedule</h1>
      <p>Please see the below calendar for upcoming events at all locations:</p>
    </header>
    <section class="max-width fullcalendar" aria-label="Google Calendar">
      <div class="remove-on-data-load">
        To view upcoming events, please visit the
        <a class="arrow" href="https://calendar.google.com/calendar/embed?src=ts55m4cl42cg66bto9miv8vdcs%40group.calendar.google.com">AVC Google Calendar</a>
      </div>
    </section>
  </main>

  <?php include "template/footer.php"?>

  <script src="libraries/jquery-3.3.1.min.js"></script>
  <script src="libraries/moment.min.js"></script>
  <script src="libraries/fullcalendar/fullcalendar.min.js"></script>
  <link rel="stylesheet" href="libraries/fullcalendar/fullcalendar.min.css" />
  <script src="libraries/fullcalendar/gcal.js"></script>
  <script src="libraries/featherlight/release/featherlight.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.min.css">
  
  <script src="scripts/calendars.js"></script>
</body>
</html>
