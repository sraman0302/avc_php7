/**
 * @file Initializes all calendar widgets (FullCalendar and the
 * aside.calendar).
 */

/**
 * Calendar-related options.
 * @constant
 * @readonly
 * @global
 */
const CALENDAR_OPTIONS = {
  /** Google API key for use with the fullCalendar library. */
  GCAL_LIB_API_KEY: 'AIzaSyCYlI6lXCMHo8tjGgRqSXrbOiApB4Rg4LA',
  /** Google API key for use with the Google Calendar URL api. */
  GCAL_URL_API_KEY: 'AIzaSyAyn_2Ne3E109dYRcPosPp2Mzq2rvNiYvw',
  /** ID of the official AVC Public Google Calendar. */
  AVC_MAIN_CALENDAR_ID: 'ts55m4cl42cg66bto9miv8vdcs@group.calendar.google.com',
  /** Number of events to show in an aside.calendar widget. */
  ASIDE_CALENDAR_EVENTS_SHOWN: 3,
  /** Class added to all FullCalendar events, for styling. */
  FULLCALENDAR_EVENT_CLASS: 'gcal-event',
};

let asideCalendarElements = document.querySelectorAll('aside.calendar');
let fullCalendarElements = document.getElementsByClassName('fullcalendar');

if (!asideCalendarElements.length && !fullCalendarElements.length) {
  console.warn('No elements relating to the calendars.js file were found'
      + ' on the page. You can remove this file to improve performance.');
}

// Initialize aside.calendar widgets
if (asideCalendarElements.length) {
  if (typeof moment !== 'function') {
    console.error('The Moment library must be included for aside.calendar'
        + ' widgets to load.');
  } else {
    for (let i = 0; i < asideCalendarElements.length; i++) {
      initAsideCalendar(asideCalendarElements[i]);
    }
  }
}

// Initialize FullCalendar widgets
if (fullCalendarElements.length) {
  if (typeof $ !== 'function') {
    console.error('The jQuery library must be included for fullcalendar'
        + ' embedded calendars to load.');
  } else if (typeof moment !== 'function') {
    console.error('The Moment library must be included for fullcalendar'
        + ' embedded calendars to load.');
  } else if (typeof $.fullCalendar !== 'object') {
    console.error('The FullCalendar library must be included for FullCalendar'
        + ' embedded calendars to load.');
  } else if (typeof $.featherlight !== 'function') {
    console.warn('The Featherlight library must be included for FullCalendar'
        + ' event popups to work.');
  } else {
    for (let i = 0; i < fullCalendarElements.length; i++) {
      initFullCalendar(fullCalendarElements[i]);
    }
  }
}

/**
 * Initialize our small 'Upcoming Events' custom widget by async loading data
 * and inserting upcoming event information. Also removes any
 * `.remove-on-data-load` elements if successful.
 * @param {HTMLElement} asideElement The element to add the upcoming events to.
 * @requires /libraries/moment.min.js
 */
function initAsideCalendar(asideElement) {
  let today = new Date().toISOString();
  let url = '';
  {
    let euc = encodeURIComponent;
    url =
        'https://www.googleapis.com/calendar/v3/calendars/'
        + euc(CALENDAR_OPTIONS.AVC_MAIN_CALENDAR_ID)
        + '/events?maxResults='
        + euc(CALENDAR_OPTIONS.ASIDE_CALENDAR_EVENTS_SHOWN)
        + '&orderBy=startTime&showDeleted=false&singleEvents=true&timeMin='
        + euc(today)
        + '&key='
        + euc(CALENDAR_OPTIONS.GCAL_URL_API_KEY);
  }

  let request = new XMLHttpRequest();

  request.addEventListener('load', function handleAsideCalendarDataLoad() {
    let data = JSON.parse(this.responseText);

    if (data.error) {
      logLoadError(
        'aside.calendar widget', data.error.code, data.error.message
      );
    } else {
      populateAsideCalendar(asideElement, data.items);
      removePlaceholders(asideElement);
    }
  });

  request.addEventListener('error', function handleAsideCalendarDataError() {
    logLoadError('aside.calendar widget', this.status, this.statusText);
  });

  request.open('GET', url);
  request.send();
}

/**
 * Build and add the HTML for each event to the provided element.
 * @param {HTMLElement} asideElement The element to add the upcoming events to.
 * @param {GCalEvent[]} events Array of events to process and add.
 * @return {HTMLElement} Filled asideElement.
 * @requires /libraries/moment.min.js
 */
function populateAsideCalendar(asideElement, events) {
  events.forEach(function(event) {
    let eventSection =
        createElementWithOptions('section', '', 'event-container');

    let eventLink = createElementWithOptions('a', '', 'event');
    eventLink.href = event.htmlLink;

    let eventName = createElementWithOptions('h1', event.summary, 'name');

    let parsedStart = /** @type {MomentObject} */ moment(event.start.dateTime);
    let parsedEnd = /** @type {MomentObject} */ moment(event.end.dateTime);

    let formattedDate = /** @type {string} */ (parsedStart.format('MMM D'));
    let eventDate = createElementWithOptions('time', formattedDate, 'date');

    let formattedStartTime =
        /** @type {string} */ (parsedStart.format('H:mm A'));
    let formattedEndTime = /** @type {string} */ (parsedEnd.format('H:mm A'));
    let eventTime = createElementWithOptions(
        'time', formattedStartTime + ' - ' + formattedEndTime, 'time'
    );

    eventLink.appendChild(eventName);
    eventLink.appendChild(eventDate);
    eventLink.appendChild(eventTime);
    eventSection.appendChild(eventLink);

    asideElement.appendChild(eventSection);
  });

  return asideElement;
}

/**
 * Initialize the embedded fullcalendar widget. Want event popups? See
 * `openFullCalendarEventPopup` requires. Make sure incluides are in this order.
 * @param {HTMLElement} fullCalendarElement The element to inset the
 * fullcalendar widget into.
 * @requires /libraries/jquery-3.3.1.min.js
 * @requires /libraries/moment.min.js
 * @requires /libraries/fullcalendar/fullcalendar.min.js
 * @requires /libraries/fullcalendar/fullcalendar.min.css
 * @requires /libraries/fullcalendar/gcal.js
 * @return {HTMLElement} The filled fullCalendarElement.
 */
function initFullCalendar(fullCalendarElement) {
  let fullCalendarOptions = {
    googleCalendarApiKey: CALENDAR_OPTIONS.GCAL_LIB_API_KEY,
    events: {
      googleCalendarId: CALENDAR_OPTIONS.AVC_MAIN_CALENDAR_ID,
      className: CALENDAR_OPTIONS.FULLCALENDAR_EVENT_CLASS,
    },
    weekends: false,
    header: {
      // Less buttons on mobile since week and calendar views are useless
      right: 'month,list,agendaWeek today prev,next',
    },
    footer: {
      left: 'prev',
      right: 'next',
    },
    buttonText: {
      month: 'Calendar',
      list: 'List',
      today: 'Today',
      agendaWeek: 'Week',
    },
    height: 'auto',
    eventLimit: 6,
    eventColor:
        getComputedStyle(document.body).getPropertyValue('--accent-primary')
        || GLOBAL_OPTIONS.FALLBACK_COLORS.PRIMARY,
    eventTextColor: '#fff',
    businessHours: {
      dow: [1, 2, 3, 4, 5],
      start: '09:00',
      end: '17:00',
    },
    eventClick: handleFullCalendarEventClick,
    // Change default view on mobile to list
    defaultView: useMobileLayout() ? 'list' : 'month',
  };

  let fullCalendar = $(fullCalendarElement).fullCalendar(fullCalendarOptions);
  if (typeof fullCalendar === 'object') {
    removePlaceholders(fullCalendarElement);
  }

  return fullCalendarElement;
}

/**
 * Handle a click on a FullCalendar event item, preventing redirects and opening
 * a popup if necessary.
 * @param {FullCalendarEvent} event The target FullCalendar event object.
 * @return {boolean} false
 */
function handleFullCalendarEventClick(event) {
  if (typeof $.featherlight !== 'function') {
    console.error('The Featherlight library must be included for'
    + ' event popups to appear.');
  } else if (event.url) {
    openFullCalendarEventPopup(event);
  }

  return false;
}

/**
 * Open a fullCalendar event as an inline popup on the page. Thes requires are
 * in addition to initFullCalendar requires.
 * @param {FullCalendarEvent} event The FullCalendar event to show.
 * @requires /libraries/featherlight/release/featherlight.min.js
 * @requires /libraries/featherlight/release/featherlight.min.css
 */
function openFullCalendarEventPopup(event) {
  let eventPopupContainer = document.createElement('section');
  eventPopupContainer.classList.add('default-body', 'fullcalendar_event_popup');

  let eventTitle = createElementWithOptions('h3', event.title);

  let eventData = document.createElement('small');

  let formattedStartTime = event.start.format('MMMM Do, h:mm a');
  let formattedEndTime = event.end.format('MMMM Do, h:mm a');
  let eventTime = createElementWithOptions(
    'time', formattedStartTime + '-' + formattedEndTime
  );

  let eventLocation = document.createElement('div');
  let eventLocationLabel =
      createElementWithOptions('span', 'Location:', 'not-bold');
  let eventLocationText = document.createTextNode(event.location || '');
  eventLocation.appendChild(eventLocationLabel);
  eventLocation.appendChild(eventLocationText);

  eventData.appendChild(eventTime);
  eventData.appendChild(eventLocation);

  let eventDescription = document.createElement('p');
  eventDescription.innerHTML = event.description || '';

  let eventLink = createElementWithOptions('a', 'Google Calendar »');
  eventLink.href = event.url;
  let moreInfo =
      createElementWithOptions('p', 'For more information, open in ');
  moreInfo.appendChild(eventLink);

  eventPopupContainer.appendChild(eventTitle);
  eventPopupContainer.appendChild(eventData);
  eventPopupContainer.appendChild(eventDescription);
  eventPopupContainer.appendChild(moreInfo);

  $.featherlight(eventPopupContainer.outerHTML, {type: 'html'});
}

/**
 * @typedef {Object} GCalEventTimeInfo Time data for a Google Calendar event.
 * @prop {string} dateTime RFC3339 timestamp.
 * @prop {string} timeZone Time zone description.
 */

/**
 * @typedef {Object} GCalEvent A Google Calendar event, loaded from the Calendar
 * web API.
 * *Note*: This is not a complete typedef, see link for more properties.
 * @prop {string} htmlLink Link to event details.
 * @prop {GCalEventTimeInfo} start Start time information.
 * @prop {GCalEventTimeInfo} end End time information.
 * @prop {string} location Location where the event will be held.
 * @prop {string} summary Name of the event.
 * @see {@link https://developers.google.com/calendar/v3/reference/events}
 */

/**
 * @typedef {Object} FullCalendarEvent A FullCalendar event object.
 * *Note*: This is not a complete typedef, see link for more properties.
 * @prop {string} url Link to event details.
 * @prop {MomentObject} start Start time information as a Moment.js object.
 * @prop {MomentObject} end End time information as a Moment.js object.
 * @prop {?string} location Location where the event will be held.
 * @prop {string} title Name of the event.
 * @prop {string} description Description of the event.
 * @see {@link https://fullcalendar.io/docs/event-object}
 */

 /**
  * @typedef {Object} MomentObject A Moment.js time object.
  * @see {@link https://momentjs.com/docs/}
  */
