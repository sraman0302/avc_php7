/**
 * @file Global / miscellaneous scripts. Keep lightweight.
 * @requires /libraries/cssvars.ponyfill.js
 */

/**
 * Global options.
 * @constant
 * @readonly
 * @global
 */
const GLOBAL_OPTIONS = {
  /** `dataset` property name for the location of the HD image. */
  RESPONSIVE_IMAGE_SRC_ATTR: 'hdSrc',
  /** Fallbacks for browsers that don't support CSS variables. */
  FALLBACK_COLORS: {
    /** Primary accent color. */
    PRIMARY: '#10805e',
    /** Secondary accent color. */
    SECONDARY: '#9ccb3b',
  },
};

// #region Initalize menus
let siteMenuElement = document.getElementById('site-menu');
let secondaryMenuElements =
    document.getElementsByClassName('secondary-navigation');

if (siteMenuElement) initMenu(siteMenuElement);
if (secondaryMenuElements.length) {
  for (let i = 0; i < secondaryMenuElements.length; i++) {
    initMenu(secondaryMenuElements[i]);
  }
}
// #endregion

// #region Load responsive images
let responsiveImageElements =
    document.getElementsByClassName('responsive-resolution');
let responsiveBackgroundImageElements =
    document.getElementsByClassName('responsive-resolution-bg');

if (responsiveImageElements.length
    || responsiveBackgroundImageElements.length) {
  window.addEventListener('load', function() {
    // Have to use normal for loops because IE doesn't support for...of
    // Loading higher res images is a basic site function that needs to work
    for (let i = 0; i < responsiveBackgroundImageElements.length; i++) {
      loadResponsiveImage(responsiveBackgroundImageElements[i], true);
    }

    for (let i = 0; i < responsiveImageElements.length; i++) {
      loadResponsiveImage(responsiveImageElements[i]);
    }
  });
}
// #endregion

// #region Init CSS Variables ponyfill
cssVars();
// #endregion

// #region Object.assign polyfill
if (typeof Object.assign != 'function') {
  Object.assign = function(target) {
    'use strict';
    if (target == null) {
      throw new TypeError('Cannot convert undefined or null to object');
    }

    target = Object(target);
    // eslint-disable-next-line no-var
    for (var index = 1; index < arguments.length; index++) {
      // eslint-disable-next-line prefer-rest-params, no-var
      var source = arguments[index];
      if (source != null) {
        for (var key in source) { // eslint-disable-line no-var
          if (Object.prototype.hasOwnProperty.call(source, key)) {
            target[key] = source[key];
          }
        }
      }
    }
    return target;
  };
}
// #endregion

/**
 * Initialize a menu by adding event listeners.
 * @param {HTMLElement} menuElement The menu element.
 */
function initMenu(menuElement) {
  menuElement.addEventListener('click', handleMenuClick);
  menuElement.addEventListener('touchstart', handleMenuClick);
}

/**
 * Handle a touch or tap on a menu by opening the menu and adding a listener
 * for the document to close the menu.
 * @param {Event} event The click or tap event that opened the menu.
 */
function handleMenuClick(event) {
  event.stopPropagation();

  let menu = /** @type {HTMLElement} */ (event.currentTarget);
  menu.classList.add('expanded');

  document.addEventListener('click', function() {
    menu.classList.remove('expanded');
  }, {once: true});
}

/**
 * Load the high-resolution version of the image.
 * @param {HTMLElement} imageElement The element with the image.
 * @param {boolean} [isBackgroundImage] If true, swaps background image instead.
 */
function loadResponsiveImage(imageElement, isBackgroundImage) {
  let highResSrc =
      imageElement.dataset[GLOBAL_OPTIONS.RESPONSIVE_IMAGE_SRC_ATTR];
  let highResImage = new Image();

  highResImage.addEventListener('load', function() {
    if (isBackgroundImage) {
      replaceBackgroundImage(imageElement, highResImage);
    } else {
      replaceImage(imageElement, highResImage);
    }
  });

  highResImage.src = highResSrc;
}

/**
 * Replace an HTML image.
 * @param {HTMLImageElement} oldImage The image to replace.
 * @param {HTMLImageElement} newImage The new image to replace `oldImage` with.
 */
function replaceImage(oldImage, newImage) {
  oldImage.src = newImage.src;
}

/**
 * Replace a CSS background image.
 * @param {HTMLElement} oldImageContainer The image to replace.
 * @param {HTMLImageElement} newImage The new image to replace with.
 */
function replaceBackgroundImage(oldImageContainer, newImage) {
  oldImageContainer.style.backgroundImage = 'url(' + newImage.src + ')';
}

/**
 * Log an HTTP request failure / load error / authentication error.
 * @param {string} dataFor Description of what the data was being loaded for.
 * @param {number} statusCode HTTP status code related to the failure.
 * @param {string} statusMethod Description of the HTTP status code.
 */
function logLoadError(dataFor, statusCode, statusMethod) {
  console.error('Loading failed for ' + dataFor + ' data with status: '
      + statusCode + ' (' + statusMethod + ')');
}

/**
 * Create a DOM element with a textNode already appended.
 * @param {string} tagName The name of an element.
 * @param {string} [textContent=''] The text to insert into the element.
 * @param {?string} [className=null] The class to add to the element.
 * @return {HTMLElement} The element, like
 * `<tagName class="className">textContent</tagName>`.
 */
function createElementWithOptions(tagName, textContent, className) {
  // IE doesn't support default function parameters
  className = className === undefined ? null : className;
  textContent = textContent === undefined ? '' : textContent;

  let textNode = document.createTextNode(textContent);
  let element = document.createElement(tagName);
  element.appendChild(textNode);
  if (className) element.classList.add(className);
  return element;
}

/**
 * Remove any placeholder elements from the container element. Ie, on data load,
 * remove loading messages.
 * @param {HTMLElement} container The element to remove placeholders from.
 * @param {string} [placeholderClass='remove-on-data-load'] The class shared by
 * the placeholder elements.
 * @return {HTMLElement} The container.
 */
function removePlaceholders(
    container,
    placeholderClass) {
  // IE doesn't support default function parameters
  placeholderClass =
      placeholderClass === undefined ? 'remove-on-data-load' : placeholderClass;

  let placeholderElements = container.querySelectorAll('.' + placeholderClass);
  for (let i = 0; i < placeholderElements.length; i++) {
    container.removeChild(placeholderElements[i]);
  }
  return container;
}

/**
 * Check if a small screen is being used. You should probably use CSS or detect
 * touch instead if  possible but sometimes this is necessary. Note that this
 * returns a value based on rem, which is based on the root font size, set in
 * the CSS.
 * @param {number} [threshhold=35] Maximum screen size (in rem) to return true
 * for.
 * @return {boolean} True if screen size is less than `threshhold`rem.
 */
function useMobileLayout(threshhold) {
  // IE doesn't support default function parameters
  threshhold = threshhold === undefined ? 35 : threshhold;

  return window.innerWidth <= threshhold * parseFloat(
      getComputedStyle(document.documentElement).fontSize);
}

/**
 * Google APIs.
 * @namespace google
 * @see {@link https://developers.google.com|Google Developers site}
 */
