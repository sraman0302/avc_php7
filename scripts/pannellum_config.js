/**
 * @file Initilizes the interactive embedded panoramas.
 */

/**
 * Panorama-related options.
 * @constant
 * @readonly
 * @global
 */
const PANNELLUM_OPTIONS = {
  /** `dataset` property name for the source of the pano image. Do not include
   * the 'data-' prefix. */
  SRC_PROPERTY: 'src',
  /** Whether or not to auto-load the panoramas on page load. */
  AUTO_LOAD: true,
  /** The speed of auto-rotation in degrees per second (0 to stop). */
  AUTO_ROTATE: -1,
  /** Seconds of inactivity after which to auto-rotate. */
  INACTIVITY_DELAY: 5,
  /** Whether or not to show the compass. */
  SHOW_COMPASS: false,
};

let panoElements = document.getElementsByClassName('pano');

if (!panoElements.length) {
  console.warn('No elements relating to the pannellum_config.js file were found'
      + ' on the page. You can remove this file and panellum.js to improve'
      + ' performance.');
} else if (typeof pannellum !== 'object') {
  console.error('The Pannellum library must be included for embedded panoramas'
      + ' to load.');
} else {
  for (let i = 0; i < panoElements.length; i++) {
    initPannellum(panoElements[i]);
  }
}

/**
 * Initialize Pannellum embedded interactive panoramas.
 * @param {HTMLElement} containerElement The container for the panorama.
 * @requires /libraries/pannellum/pannellum.js
 * @requires /libraries/pannellum/pannellum.css
 */
function initPannellum(containerElement) {
  let id = containerElement.id;
  let src = containerElement.dataset[PANNELLUM_OPTIONS.SRC_PROPERTY];
  let type = containerElement.dataset.type;

  pannellum.viewer(id, {
    'type': type,
    'panorama': src,
    'autoLoad': PANNELLUM_OPTIONS.AUTO_LOAD,
    'autoRotate': PANNELLUM_OPTIONS.AUTO_ROTATE,
    'autoRotateInactivityDelay': PANNELLUM_OPTIONS.INACTIVITY_DELAY,
    'compass': PANNELLUM_OPTIONS.SHOW_COMPASS,
    'mouseZoom': 'fullscreenonly',
  });
}
