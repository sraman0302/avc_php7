/**
 * @file Initialize and configure the main page slideshow.
 */

/**
 * Slideshow-related options.
 * @constant
 * @readonly
 * @global
 */
const SLIDESHOW_OPTIONS = {
  /** Slide duration, seconds. */
  SLIDE_DURATION: 5,
};

let slideshowElements = document.querySelectorAll('.slideshow');

if (!slideshowElements.length) {
  console.warn('No elements relating to the slideshow.js file were found'
      + ' on the page. You can remove this file to improve performance.');
} else {
  for (let i = 0; i < slideshowElements.length; i++) {
    initSlideshow(slideshowElements[i]);
  }
}

/**
 * Initalize the slideshow and bind events.
 * @param {HTMLElement} slideshowElement The container for the slideshow.
 */
function initSlideshow(slideshowElement) {
  slideshowElement.dataset.mouseIsOver = 'false';

  slideshowElement.addEventListener('mouseenter', handleSlideshowMouseenter);
  slideshowElement.addEventListener('mouseleave', handleSlideshowMouseleave);

  startSlideshow(slideshowElement);
}

/**
 * Handle slideshow mouseenter by updating the `data-mouseIsOver` attribute.
 * @param {Event} event The mouseenter event.
 */
function handleSlideshowMouseenter(event) {
  event.target.dataset.mouseIsOver = 'true';
}

/**
 * Handle slideshow mouseleave by updating the `data-mouseIsOver` attribute.
 * @param {Event} event The mouseleave event.
 */
function handleSlideshowMouseleave(event) {
  event.target.dataset.mouseIsOver = 'false';
}

/**
 * Show the next slide in the slideshow.
 * @param {HTMLElement} slideshowElement The initialized slideshow's container.
 */
function showNextSlide(slideshowElement) {
  if (!slideshowElement.dataset.mouseIsOver
      || slideshowElement.dataset.mouseIsOver === 'false') {
    /** The currently shown slide. */
    let currentSlide = slideshowElement.querySelector('.slide.current');
    /** The next slide in the sequence, or the first slide. */
    let nextSlide = currentSlide.nextElementSibling
        || slideshowElement.querySelector('.slide');

    currentSlide.classList.remove('current');
    currentSlide.setAttribute('aria-hidden', 'true');
    nextSlide.classList.add('current');
    nextSlide.setAttribute('aria-hidden', 'false');
  }
}

/**
 * Shows the next slide in the slideshow, then recursively calls itself after
 * the specified duration.
 * @param {HTMLElement} slideshowElement The initialized slideshow's container.
 */
function startSlideshow(slideshowElement) {
  showNextSlide(slideshowElement);

  setTimeout(function() {
    startSlideshow(slideshowElement);
  }, SLIDESHOW_OPTIONS.SLIDE_DURATION * 1000);
}
