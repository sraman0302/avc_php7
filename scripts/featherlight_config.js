/**
 * @file Initilizes and configures Featherlight inline popups.
 * Featherlight is included on all pages because it is used for most images and
 * some external links.
 * @requires /libraries/jquery-3.3.1.min.js
 * @requires /libraries/featherlight/release/featherlight.min.js
 * @requires /libraries/featherlight/release/featherlight.min.css
 * @requires /libraries/featherlight/release/featherlight.gallery.min.js
 * Only needed for galleries.
 * @requires /libraries/featherlight/release/featherlight.gallery.min.css
 * Only needed for galleries.
 */

/**
 * Featherlight-related options.
 * @constant
 * @readonly
 * @global
 */
const FEATHERLIGHT_OPTIONS = {
  /** The attribute used to provide image captions to featherlight. */
  IMG_DESCRIPTION_ATTR: 'alt',
};

let featherlightElements =
    document.querySelectorAll('[data-featherlight]');
let featherlightGalleryElements =
    document.querySelectorAll('[data-featherlight-gallery]');

if ((featherlightElements.length || featherlightGalleryElements.length)
    && typeof $ !== 'function') {
  console.error('The jQuery library must be included for Featherlight.');
} else if (featherlightElements.length
    && typeof $.featherlight !== 'function') {
  console.error('The Featherlight library must be included for inline'
      + ' popups to show.');
} else if (featherlightGalleryElements.length
    && typeof $.featherlightGallery !== 'function') {
  console.error('The Featherlight Gallery library must be included for inline'
      + ' gallleries to function.');
} else if (featherlightElements.length && featherlightGalleryElements.length) {
  // Featherlight automatically initializes itself if data-featherlight attr'
  // is present on element.
  addFeatherlightImageCaptions();
}

/**
 * Add captions to featherlight image popups. Doesn't affect non-image popups.
 */
function addFeatherlightImageCaptions() {
  $.featherlight.prototype.afterContent = function() {
    if (
      this.$currentTarget &&
      this.$currentTarget.find('img')
          .attr(FEATHERLIGHT_OPTIONS.IMG_DESCRIPTION_ATTR)
    ) {
      let caption = this.$currentTarget.find('img')
          .attr(FEATHERLIGHT_OPTIONS.IMG_DESCRIPTION_ATTR);

      this.$instance.find('.caption').remove();
      $('<div class="caption">').text(caption)
        .appendTo(this.$instance.find('.featherlight-content'));
    }
  };
}
