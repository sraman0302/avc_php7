/**
 * @file Initilizes the Google Maps embed, specific to `about/locations.php`.
 */

/**
 * Construct a new LatLng.
 * @class
 * @classdesc Very basically mimics `google.maps.LatLng` in order to save LatLng
 * objects before the API loads. Can be used in place of the Maps version but
 * doesn't have any of the features besides a basic constructor.
 * @param {number} latitude Latitude ranges between -90 and 90 degrees,
 * inclusive.
 * @param {number} longitude Longitude ranges between -180 and 180 degrees,
 * inclusive.
 */
function LatLng(latitude, longitude) {
  /** Latitude ranges between -90 and 90 degrees, inclusive. */
  this.lat = latitude;
  /** Longitude ranges between -180 and 180 degrees, inclusive. */
  this.lng = longitude;
}

/**
 * Construct a new AvcLocation.
 * @class
 * @classdesc Respresents a location operated by the AVC.
 * @param {string} name Name of the location.
 * @param {string} letter Letter representing the location on the page.
 * @param {google.maps.LatLng} coords Coordinates of the location.
 */
function AvcLocation(name, letter, coords) {
  /** Name of the location. */
  this.name = name;
  /** Letter representing the location on the page */
  this.letter = letter;
  /** Coordinates of the location. */
  this.coords = coords;
}

/**
 * Locations Map-related options.
 * @constant
 * @readonly
 * @global
 */
const LOCATIONS_MAP_OPTIONS = {
  /** ID of the container that holds the map. */
  CONTAINER_ID: 'locations-map',
  /** Names of CSS color vars, without preceding hyphens. */
  COLOR_VARS: {
    /** Primary accent color. */
    PRIMARY: 'accent-primary',
    /** Secondary accent color. */
    SECONDARY: 'accent-secondary',
  },
  /** Zoom level of the map on load. */
  DEFAULT_MAP_ZOOM: 15,
  /** Center of the map on load. */
  CAMPUS_COORDS: new LatLng(28.0618315, -82.4213621),
  /** List of all locations that the AVC operates. Also need to edit the PHP. */
  AVC_LOCATIONS: [
    new AvcLocation(
        '3D Printing Lab',
        'A',
        new LatLng(28.060032, -82.415211)),
    new AvcLocation(
        'Advanced Visualization Auditorium',
        'B',
        new LatLng(28.060039, -82.415291)),
    new AvcLocation(
        'Visualization Computer Lab',
        'C',
        new LatLng(28.060621, -82.416082)),
    new AvcLocation(
        'Shimberg Library 3D Printing Lab',
        'D',
        new LatLng(28.064007, -82.425263)),
  ],
  /** SVG MAP_PIN path from {@link http://map-icons.com/} */
  PIN_PATH: 'M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7'
      + ' 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z',
};

/**
 * Callback function that runs when the Google Maps API loads.
 * @requires GoogleMapsApi
 * @requires /libraries/jquery-3.3.1.min.js
 * @todo Remove jQuery dependency completely
 */
function initMap() {
  /** The element that will contain the map. */
  let mapElement = document.getElementById(LOCATIONS_MAP_OPTIONS.CONTAINER_ID);
  /**
   * Primary pin color.
   * @type {string}
   */
  let primaryColor =
      getComputedStyle(document.documentElement)
          .getPropertyValue(LOCATIONS_MAP_OPTIONS.COLOR_VARS.PRIMARY)
      || GLOBAL_OPTIONS.FALLBACK_COLORS.PRIMARY;
  /**
   * Highlighted pin color.
   * @type {string}
   */
  let secondaryColor =
      getComputedStyle(document.documentElement)
          .getPropertyValue(LOCATIONS_MAP_OPTIONS.COLOR_VARS.SECONDARY)
      || GLOBAL_OPTIONS.FALLBACK_COLORS.SECONDARY;
  /** Letter font. */
  let font = getComputedStyle(document.body).getPropertyValue('font-family');

  /** Default Google Maps symbol. */
  const DEFAULT_SYMBOL = {
    path: LOCATIONS_MAP_OPTIONS.PIN_PATH,
    fillColor: primaryColor,
    fillOpacity: 1,
    strokeColor: '#fff',
    labelOrigin: new google.maps.Point(0, -30),
    scale: 0.8,
  };

  /** Google Maps symbol, hover state. */
  const HOVER_SYMBOL = Object.assign({}, DEFAULT_SYMBOL, {
    scale: 1,
    fillColor: secondaryColor,
  });

  /** Default properties of map labels. */
  const DEFAULT_LABEL = {
    color: '#fff',
    fontWeight: 'bold',
    fontFamily: font,
    fontSize: '1.3rem',
    text: 'X',
  };

  /**
   * The initialized Google Map.
   * @type {Object}
  */
  let locationsMap =
      new google.maps.Map(document.getElementById('locations-map'), {
        zoom: LOCATIONS_MAP_OPTIONS.DEFAULT_MAP_ZOOM,
        center: LOCATIONS_MAP_OPTIONS.CAMPUS_COORDS,
        minZoom: LOCATIONS_MAP_OPTIONS.DEFAULT_MAP_ZOOM - 3,
      });

  /** All markers shown on the map, with reference letters as keys. */
  let markers = /** @type {Map<string, Object>} */ (new Map());
  LOCATIONS_MAP_OPTIONS.AVC_LOCATIONS.forEach(function(location) {
    /** Initialize and save the marker at the same time. */
    markers.set(location.letter, new google.maps.Marker({
      map: locationsMap,
      position: location.coords,
      title: location.name,
      label: Object.assign({}, DEFAULT_LABEL, {text: location.letter}),
      icon: DEFAULT_SYMBOL,
    }));
  });

  // TODO: Remove jQuery dependency and bind events using vanilla JS

  $('[data-mapicon]').each(function labelRelatedHeader() {
    let mapicon = this.dataset.mapicon;
    let $header = $(this).find('h3');
    $header.append(' (' + mapicon + ')');
  });

  // Attach mouse events to map icons
  $('[data-mapicon]').on('mouseover', function handleIconMouseOver() {
    let iconLabel = this.dataset.mapicon;

    markers.get(iconLabel).setIcon(HOVER_SYMBOL);
    markers.get(iconLabel).setZIndex(99);
  }).on('mouseout', function handleIconMouseOut() {
    let iconLabel = this.dataset.mapicon;

    markers.get(iconLabel).setIcon(DEFAULT_SYMBOL);
    markers.get(iconLabel).setZIndex(null);
  }).on('click', function handleIconClick() {
    let iconLabel = this.dataset.mapicon;

    locationsMap.setZoom(LOCATIONS_MAP_OPTIONS.DEFAULT_MAP_ZOOM + 3);
    locationsMap.panTo(markers.get(iconLabel).position);
  });
}

/**
 * The Google Maps API library.
 * @memberof google
 * @namespace maps
 * @see {@link https://developers.google.com/maps/documentation/javascript/tutorial|Google Maps JavaScript API documentation}
 */

/**
 * Map class
 * @typedef {Object} Map
 * @memberof google.maps
 * @see {@link https://developers.google.com/maps/documentation/javascript/reference/3.exp/map|Official documentation for `google.maps.Map`}
 */

/**
 * A `LatLng` is a point in geographical coordinates: latitude and longitude.
 * @typedef {Object} LatLng
 * @memberof google.maps
 * @see {@link https://developers.google.com/maps/documentation/javascript/reference/3.exp/coordinates#LatLng|Official documentation for `google.maps.LatLng`}
 */

/**
 * A point on a two-dimensional plane.
 * @typedef {Object} Point
 * @memberof google.maps
 * @see {@link https://developers.google.com/maps/documentation/javascript/reference/3.exp/coordinates#Point|Official documentation for `google.maps.Point`}
 */

/**
 * A marker on a map.
 * @typedef {Object} Marker
 * @memberof google.maps
 * @see {@link https://developers.google.com/maps/documentation/javascript/reference/3.exp/marker|Official documentation for `google.maps.Marker`}
*/
