<!DOCTYPE html>
<html lang="en">
<!-- Use this code as a template for new pages -->
<head>
  <?php include "template/head.php";?>

  <title>Sorry, Page Not Found</title>
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "template/header.php";?>
  <?php include "template/menu.php";?>

  <main id="main-content">
    <header class="page-header max-width">
      <h1>Page Not Found (404)</h1>
    </header>
    <article class="default-body max-width">
      <p>Looks like the page you're looking for doesn't exist. If you think it should exist, please <a href="about#main-content">contact us</a> and ask about it.</p>
      <br><a href="/">HOME</a>
    </article>
  </main>

  <?php include "template/footer.php"?>
</body>
</html>
