/**
 * Sanitizes a string and replaces newline chracters with new paragraphs.
 * @param {string} input - Text to be parsed, with "/n" to represent new line.
 * @returns - HTML string.
 */
function parseParagraphText(input) {
  // Sanitizes input and replaces "/n" with "</p><p>"
  return '<p>' + input.replace(/&/g, '&amp;')
                      .replace(/</g, '&lt;')
                      .replace(/"/g, '&quot;')
                      .replace(/\\n/g, '</p><p>') + '</p>';
}

/**
 * Callback to be run after Google API auth.
 */
function runAfterGapiAuth() {
  // Boilerplate:
  const opts = {
    optionsSheetID: "16Pjdxt1MPlnP9l6xkTouc51NEq47uf1uzDZav_6uNtQ",
    updateOptionsInterval: 5*60*1000,
    reloadPageInterval: 5*60, // Minutes
    slideDuration: -1,
    calendarSlideNumber: 2,
    gallerySlideNumber: 2,
    slideTransitionDuration: 250, 
      // Don't forget to change the .slide {} CSS transition duration
    numSlides: 4,
    contact: {
      primary: {
        name: "",
        title: "",
        email: ""
      },
      secondary: {
        name: "",
        title: "",
        email: ""
      }
    },
    announcements: [{
      title: "",
      text: "",
      image: ""
    }],
    printing: {
      printPrice: "",
      minPrintCost: ""
    },
    galleryImages: [""],
    fetched: false
  },
  // All elements to be used:
  contElements = {
    announcements: Array.from(document.getElementsByClassName("announcements")),
    gallery:      document.getElementById("gallery"),
    weekCalendar: document.getElementById("weekCalendarEmbed"),
    galleryImages: Array.from(document.getElementsByClassName("gallery-image")),
    contact: {
      primary: {
        name:  document.getElementById("primaryName"),
        title: document.getElementById("primaryTitle"),
        email: document.getElementById("primaryEmail")
      },
      secondary: {
        name:  document.getElementById("secondaryName"),
        title: document.getElementById("secondaryTitle"),
        email: document.getElementById("secondaryEmail")
      }
    },
    printing: {
      printPrice: document.getElementById("printPrice"),
      minPrintCost: document.getElementById("minPrintCost")
    }
  };

  let slideItems = document.getElementsByClassName("slide"),
      pageName = document.getElementById("pageName"),
      currentSlide = opts.numSlides,
      firstRun = true;

  /**
   * Recursive slide changing function that calls itself with setTimeout.
   * (SetInterval would get too inaccurate over time and can cause other issues)
   */
  function changeSlide() {
    // Start slideshow over when end is reached
    currentSlide < opts.numSlides - 1 ? currentSlide++ : currentSlide = 0;

    let slideItems = Array.from(document.getElementsByClassName("slide")),
        hidePageName = false;

    // To pull off the fade transition, we fade everything out and then fade 
    // everything back in
    slideItems.forEach(element => element.classList.add("faded-out"));
    pageName.classList.add("faded-out");

    setTimeout(function() {
      slideItems.forEach(element => {
        if(element.classList.contains(`slide-${currentSlide}`)) {
          element.classList.remove("not-current-slide");
          if(element.classList.contains("hide-pagename")) hidePageName = true;
        } else {
          element.classList.add("not-current-slide");
        }
      });

      if(hidePageName) {
        pageName.classList.add("not-current-slide");
      } else {
        pageName.classList.remove("not-current-slide");
      }

      // When we hide and reshow the iframe, it scrolls up to the top, but 
      // since it's a schedule we want to have it scrolled to the default 
      // location, so we have to reload it every damn time we show it
      if(currentSlide == opts.calendarSlideNumber) {
        contElements.weekCalendar.src += "";
      }

      setTimeout(function() {
        slideItems.forEach(element => element.classList.remove("faded-out"));
        pageName.classList.remove("faded-out");
      }, 150);
    }, opts.slideTransitionDuration);

    // Change gallery image when slide after gallery is showing
    if(currentSlide == opts.gallerySlideNumber + 1 || firstRun) {
      let usedIndexes = [-2];
      contElements.galleryImages.forEach(imgElement => {
        
        let randImageIndex = -2;
        while(usedIndexes.indexOf(randImageIndex) !== -1) {
          randImageIndex = Math.floor(Math.random() * opts.galleryImages.length);
        }
        
        usedIndexes.push(randImageIndex);

        imgElement.style.backgroundImage = 
          `url("${opts.galleryImages[randImageIndex]}")`;
      });

      firstRun = true;
    }

    setTimeout(changeSlide, opts.slideDuration);
  };

  /**
   * Recursive status updating function that calls itself with setTimeout.
   * (SetInterval would get too inaccurate over time and can cause other issues)
   * Also reloads the page every so often.
   */
  let minutesSinceLastReload = 0;
  function updateOpenIndicator() {

    if(minutesSinceLastReload >= opts.reloadPageInterval) {
      location.reload(true);
    } else {
      minutesSinceLastReload++;
    }
    setTimeout(updateOpenIndicator, 60*1000);
  };

  /**
   * Recursive IIFE updating function that calls itself with setTimeout.
   * (SetInterval would get too inaccurate over time and can cause other issues)
   * Also calls the other functions.
   */
  (function updateOptions() {
    if(gapi.auth.authorize) {
      gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: opts.optionsSheetID,
        range: "shimberg options"
      }).then(response => {
        /// Pull options from configurator spreadsheet
        let values = response.result.values;

        // Slide Duration
        opts.slideDuration = values[1][1] * 1000;

        // Contact Information section
        opts.contact.primary.name     = values[ 4][1];
        opts.contact.primary.title    = values[ 5][1];
        opts.contact.primary.email    = values[ 6][1];
        opts.contact.secondary.name   = values[ 7][1];
        opts.contact.secondary.title  = values[ 8][1];
        opts.contact.secondary.email  = values[ 9][1];

        opts.printing.printPrice   = values[10][1];
        opts.printing.minPrintCost = values[11][1];

        // Announcements section
        for(let i = 0; i < 5; i++) {
          let rowNum = i + 19;
          if(values[rowNum] && values[rowNum][0]) {
            opts.announcements[i] = {
              title: values[rowNum][0],
              text:  parseParagraphText(values[rowNum][1]),
              // Show a transparent image to avoid 'broken image' icon
              image: values[rowNum][2].trim() || 
                "https://upload.wikimedia.org/wikipedia/commons/c/ce/Transparent.gif"
            }
          }
        }

        // Slideshow Images (gallery) section
        for(let i = 0; i < 100; i++) {
          let rowNum = i + 36;
          if(values[rowNum] && values[rowNum][0]) {
            opts.galleryImages[i] = values[rowNum][0];
          }
        }

        // only should run the first time the options are loaded
        if(!opts.fetched) {
          // These run as separate functions because each runs recursively 
          // on a different interval
          changeSlide();
          updateOpenIndicator();
        }

        if(values) opts.fetched = new Date();

        // Fill in Announcements:
        opts.announcements.forEach((anncmnt, index) => {
          // It's better to build the HTML from scratch then leave 5 empty 
          // announcements and edit them
          let anncmntContent = 
            `<h3><span>${anncmnt.title}</span></h3>
              ${anncmnt.text}<img src="${anncmnt.image}"/>`;
          contElements.announcements[index].innerHTML = anncmntContent;
        });

        // Fill in Contact Info:
        contElements.contact.primary.name.textContent    = opts.contact.primary.name;
        contElements.contact.primary.title.textContent   = opts.contact.primary.title;
        contElements.contact.primary.email.textContent   = opts.contact.primary.email;
        contElements.contact.secondary.name.textContent  = opts.contact.secondary.name;
        contElements.contact.secondary.title.textContent = opts.contact.secondary.title;
        contElements.contact.secondary.email.textContent = opts.contact.secondary.email;

        contElements.printing.printPrice.textContent      = opts.printing.printPrice;
        contElements.printing.minPrintCost.textContent   = opts.printing.minPrintCost;

        
      });
      console.log(opts);
    } else initClient();

    setTimeout(updateOptions, opts.updateOptionsInterval);
  })();
}