<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="theme-color" content="#10805e">
  <meta name="geo.region" content="US-FL">
  <meta name="geo.placename" content="Tampa">
  <meta name="og:locale" content="en_US">
  <meta name="og:site_name" content="USF Advanced Visualization Center">
  <meta name="twitter:card" content="summary_large_image">

    <base href="http://127.0.0.1:8080/edsa-AVC%201/">

  
  
  <link rel="stylesheet" href="template/resets.css">
  <link rel="stylesheet" href="template/template.css">
  <link rel="stylesheet" href="template/widgets.css">
  <link rel="shortcut icon" type="image/ico" href="template/favicon.ico"/>

  <title>USF Advanced Visualization Center - Home</title>

  <meta name="og:title" content="USF Advanced Visualization Center">
  <meta name="og:description" content="The Advanced Visualization Center staff assists students and faculty with the use of advanced technologies for the creation of visualizations for education and research.">
  <meta name="description" content="Home of the Advanced Visualization Center at the University of South Florida.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">
  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="https://avc.web.usf.edu//">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <a class="skip-to-main" href="//avc.web.usf.edu/#main-content">Skip to main content</a> 

<header>
  <div class="max-width" id="global-header" itemprop="parentOrganization" itemscope itemtype="http://schema.org/EducationalOrganization">
    <a id="global-logo" href="http://usf.edu" itemprop="url"><span itemprop="name">University of South Florida</span></a>
    <nav>
      <a href="http://my.usf.edu">MyUSF</a>
      <a href="http://directory.usf.edu/">Directory</a>
      <a href="http://www.usf.edu/it/">Information Technology</a>
    </nav>
  </div>
  <div id="site-header">
    <div class="max-width">
      <h1>
        <a href="/" itemprop="url"><span itemprop="name">Advanced Visualization Center</span></a>
      </h1>
      <h2 itemprop="parentOrganization" itemscope itemtype="http://schema.org/Organization">
        A Division of <a href="http://www.usf.edu/it/research-computing/" itemprop="url"><span itemprop="name">Research Computing</span></a>
      </h2>
    </div>
  </div>
</header>
  <nav id="site-menu">
  <div class="max-width">
    <h2>Navigation</h2>
    <a href="/">Home</a>
    <a href="schedule.php">Schedule</a>
    <a href="resources/index.php">Resources</a>
    <a href="showcase/index.php">Showcase</a>
    <a href="resources/3d_printing.php">3D Printing</a>
    <a href="about/index.php">About Us</a>
  </div>
</nav>

  <main id="main-content">
    <article class="slideshow max-width" aria-live="polite" aria-label="Slideshow">
            <section class="slide responsive-resolution-bg current" aria-hidden="false" style="background-image:url(libraries/phpThumb/phpThumb.php?src=..%2F..%2Fmedia%2F3D_printing%2Fresin_bug.jpg&amp;w=500&amp;h=500&amp;q=40&amp;hash=f9f805fc280440ab970780fb876d6077);" data-hd-src="media/3D_printing/resin_bug.jpg">
        <img height="300" class="sr-only" src="libraries/phpThumb/phpThumb.php?src=..%2F..%2Fmedia%2F3D_printing%2Fresin_bug.jpg&amp;h=300&amp;q=50&amp;hash=b21a6a04a6dd1fd6a9a395beca1e998f" alt="Smooth translucent 3D printed insect.">
        <div class="slide-info">
          <a href="resources/3d_printing.php">
            <h3>3D Printing</h3>
            <p>The AVC offers advanced 3D Printing capabilities to students and faculty.</p>
          </a>
        </div>
      </section>

      <section class="slide responsive-resolution-bg" aria-hidden="true" style="background-image:url(libraries/phpThumb/phpThumb.php?src=..%2F..%2Fshowcase%2Fmedia%2Fvisualization_wall%2Fdino_model_wall.jpg&amp;w=500&amp;h=500&amp;q=40&amp;hash=fe381652959bca84f079a9a3046b71ac);" data-hd-src="showcase/media/visualization_wall/dino_model_wall.jpg">
        <img height="300" class="sr-only" src="libraries/phpThumb/phpThumb.php?src=..%2F..%2Fmedia%2F3D_printing%2Fresin_bug.jpg&amp;h=300&amp;q=50&amp;hash=b21a6a04a6dd1fd6a9a395beca1e998f" alt="3D mesh render of dinosaur head, displayed on Visualization Wall.">
        <div class="slide-info">
          <a href="resources/rooms.php#auditorium">
            <h3>Visualization Wall</h3>
            <p>The Visualization Wall is a high-resolution, 3D-capable display for classes and presentations.</p>
          </a>
        </div>
      </section>

      <section class="slide responsive-resolution-bg" aria-hidden="true" style="background-image:url(libraries/phpThumb/phpThumb.php?src=..%2F..%2Fshowcase%2Fmedia%2Fcomputer_lab%2Fstudents.jpg&amp;w=500&amp;h=500&amp;q=40&amp;hash=f4f97fe331b957b96fd575269c8a58d9);" data-hd-src="showcase/media/computer_lab/students.jpg">
        <img height="300" class="sr-only" src="libraries/phpThumb/phpThumb.php?src=..%2F..%2Fmedia%2F3D_printing%2Fresin_bug.jpg&amp;h=300&amp;q=50&amp;hash=b21a6a04a6dd1fd6a9a395beca1e998f" alt="Students collaborating on class project in AVC Computer Lab.">
        <div class="slide-info">
          <a href="resources/rooms.php#computerlab">
            <h3>XR Lab</h3>
            <p>The Advanced Visualization Center XR Lab is available for general and classroom usage.</p>
          </a>
        </div>
      </section>
    </article>

    <article class="quick-links max-width" aria-label="Quick Links">
      <div> 
        <section>
          <a href="resources/3d_printing.php">
            <h3>3D Printing</h3>
            <p>View information about our 3D printing services and capabilities.</p>
          </a>
        </section>
        <section>
          <a href="resources/rooms.php">
            <h3>Rooms</h3>
            <p>Learn about and reserve teaching and presenting rooms.</p>
          </a>
        </section>
        <section>
          <a href="resources/equipment.php">
            <h3>Equipment</h3>
            <p>Check out visualization and imaging equipment available for loan.</p>
          </a>
        </section>
        <section>
          <a href="showcase/projects.php">
            <h3>Projects</h3>
            <p>Discover the AVC's latest projects and accomplishments.</p>
          </a>
        </section>
      </div>
    </article>

    <section class="aside_2-3 max-width" aria-label="Home">
      <article class="text-content">
        <h2>About Us</h2>
        <p>The Advanced Visualization Center staff assists students and faculty with the use of advanced technologies for the creation of visualizations for education and research. We also provide training on advanced software applications and data visualization techniques. The center supports the advancement of technology in education with the ultra-high resolution <a href="resources/rooms.php#auditorium">Advanced Visualization Wall</a>, with remote access to <a href="http://www.usf.edu/it/research-computing/what-is-rc/comp_resources.aspx">USF's High Performance Computing Cluster</a>, a <a href="resources/3d_printing.php">3D Printing Lab</a> with over 40 printers and the <a href="resources/rooms.php#computerlab">Visualization Computer Lab</a>. We continue to explore and develop innovative practices in 3D Modeling, 3D Printing, Visual Graphics, Animation, Augmented and Virtual Reality, Data Analysis and Interactive Applications. The Advanced Visualization Center is part of <a href="http://www.usf.edu/it/">USF Information Technology</a>, <a href="http://www.usf.edu/it/research-computing/">Research Computing</a>.</p>
      </article>
      <aside class="calendar">
        <h2 class="accent-header"><span>Upcoming Events</span></h2>
        <br class="remove-on-data-load">
        <p class="remove-on-data-load">To view upcoming events, see our <a href="https://calendar.google.com/calendar/embed?src=ts55m4cl42cg66bto9miv8vdcs%40group.calendar.google.com&ctz=America%2FNew_York">Google Calendar</a>.</p>
      </aside>
    </section>
  </main>

    <footer class="max-width">
    <div id="site-footer">
      <div id="site-info">
        <a href="http://usf.edu">
          <img src="template/footer-logo.png" alt="University of South Florida Logo">
        </a>
        <div>
          <address>
            <div>Copyright © 2018, University of South Florida Advanced Visualization Center. All rights reserved.</div>
            <div><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" id="schema-avc-address"><span itemprop="streetAddress">12010 USF Cherry Drive, CMC147</span>, <span itemprop="addressLocality">Tampa</span>, <span itemprop="addressRegion">FL</span> <span itemprop="postalCode">33620</span>, <span itemProp="addressCountry">USA</span></span></div>
            <div>This website is maintained by the <a href="about/">Advanced Visualization Center (AVC)</a>.</div>
          </address>
          <nav>
              <a href="about/">Contact AVC</a>
              <a href="http://www.usf.edu/about-usf/contact-usf.aspx" target="_blank">Contact USF</a>
              <a href="http://www.usf.edu/about-usf/visit-usf.aspx" target="_blank">Visit USF</a>
              <a href="https://github.com/usf-avc/public-website/issues" target="_blank">Report Site Issues</a>
          </nav>
        </div>    
      </div>
    </div>
  </footer>

  <script src="libraries/cssvars.ponyfill.js"></script>
  <script src="scripts/global.js"></script>
  
  <script src="libraries/moment.min.js"></script>
  
  <script src="scripts/calendars.js"></script>
  <script src="scripts/slideshow.js"></script>
</body>
</html>