<?php
  header('Content-Type:text/plain');
?>

Disallow: /libraries/
Disallow: /template/
Disallow: /resources/template/
Disallow: /about/template/
Disallow: /showcase/template/
Sitemap: <?php include "template/base.php"; ?>sitemap.php