<?xml version="1.0" encoding="UTF-8"?>
<?php
  header('Content-Type:application/xml');
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

  <url>
    <loc><?php include "template/base.php"; ?></loc>
    <changefreq>monthly</changefreq>
    <priority>0.8</priority>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>schedule.php</loc>
    <priority>0.2</priority>
  </url>

  <url>
    <loc><?php include "template/base.php"; ?>about</loc>
    <priority>0.6</priority>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>about/locations.php</loc>
    <priority>0.6</priority>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>about/staff.php</loc>
  </url>

  <url>
    <loc><?php include "template/base.php"; ?>resources</loc>
    <priority>0.3</priority>
    <changefreq>yearly</changefreq>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>resources/3d_printing.php</loc>
    <priority>0.8</priority>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>resources/services.php</loc>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>resources/equipment.php</loc>
    <priority>0.4</priority>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>resources/rooms.php</loc>
    <priority>0.6</priority>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>resources/software.php</loc>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>resources/videos.php</loc>
  </url>

  <url>
    <loc><?php include "template/base.php"; ?>showcase</loc>
    <priority>0.3</priority>
    <changefreq>yearly</changefreq>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>/showcase/edu_experiences.php</loc>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>/showcase/projects.php</loc>
  </url>
  <url>
    <loc><?php include "template/base.php"; ?>/showcase/gallery.php</loc>
  </url>

</urlset>