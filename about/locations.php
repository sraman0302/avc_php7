<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Locations | About Us | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Locations">
  <meta name="og:description" content="Information about locations operated by the Advanced Visualization Center at USF.">
  <meta name="description" content="Information about locations operated by the Advanced Visualization Center at USF.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="<?php echo $root ?>/about/staff.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <article>
        <h2>Locations and Hours</h2>
        <p class="no-justify">The AVC operates four separate locations. Our main office is located above the staircase in the Advanced Visualization Auditorium.
        <figure>
          <figcaption>
            <h3 class="accent-header"><span>Map</span></h3>
          </figcaption>
          <div class="embed-map" id="locations-map">
            <noscript>
              <img height="390" width="700" src="about/locations/map.png"                
                alt="Map of AVC locations on the USF campus." class="static-replacement">
            </noscript>
          </div>
        </figure>
        <div class="responsive-list wide-elements map-grid">
          <section data-mapicon="A" itemprop="location" itemscope itemtype="http://schema.org/Place">
            <h3 itemprop="name">Advanced Visualization Auditorium</h3>
            <p itemprop="description" class="no-justify">The auditorium has been established and equipped to serve as a teaching facility. It is home to our Visualization Wall (an 8-ft × 14-ft digital display) and our main office.</p>
            <h4 itemprop="address">CMC 147</h4>
            <span class="hours" itemprop="openingHours" content="Mo-Fr 09:00-17:00">9:00 am - 5:00 pm, Monday - Friday</span>
            <br><a href="resources/rooms.php#auditorium" itemprop="url">Reservation Information</a>
          </section>
          <section data-mapicon="B" itemprop="location" itemscope itemtype="http://schema.org/Place">
            <h3 itemprop="name">3D Printing Lab</h3>
            <p itemprop="description" class="no-justify">The 3D Printing lab is home to more than 45 3D Printers, available for student and faculty use for just $0.06 / gram of filament.</p>
            <h4 itemprop="address">CMC 153</h4>
            <span class="hours" itemprop="openingHours" content="Mo-Fr 09:00-17:00">9:30 am - 5:00 pm, Monday - Friday</span>
            <br><a href="resources/3d_printing.php" itemprop="url">3D Printing Information</a>
          </section>
          <section data-mapicon="C" itemprop="location" itemscope itemtype="http://schema.org/Place">
            <h3 itemprop="name">Visualization Computer Lab</h3>
            <p itemprop="description" class="no-justify">The Computer Lab contains a number of high-performance Windows machines for developing and running advanced visualizations. It is available on a first-come, first-served basis when not reserved.</p>
            <h4 itemprop="address">SCA 222</h4>
            <span class="hours" itemprop="openingHours" content="Mo-Fr 09:00-17:00">9:30 am - 5:00 pm, Monday - Friday</span>
            <br><a href="resources/rooms.php#computerlab" itemprop="url">Reservation & Usage Information</a>
          </section>
          <section data-mapicon="D" itemprop="location" itemscope itemtype="http://schema.org/Place">
            <h3 itemprop="name">Shimberg Library 3D Printing Lab</h3>
            <p itemprop="description" class="no-justify">The Shimberg Library 3D Printing Lab is our newest location, offering similar services to our other Lab but at a more convenient location for USF Health students and faculty.</p>
            <h4 itemprop="address">Shimberg Library (USF Health Campus)</h4>
            <span class="hours" itemprop="openingHours" content="Mo-Fr 10:00-187:00">10:00 am - 6:00 pm, Monday - Friday</span>
            <br><a href="resources/3d_printing.php" itemprop="url">3D Printing Information</a>
          </section>
        </div>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>

  <script src="libraries/jquery-3.3.1.min.js"></script>
  <script src="scripts/locations_map.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyn_2Ne3E109dYRcPosPp2Mzq2rvNiYvw&callback=initMap"></script>
</body>
</html>
