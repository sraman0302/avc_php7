<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Contact | About Us | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Contact Information">
  <meta name="og:description" content="Contact information for the USF Advanced Visualization Center at USF.">
  <meta name="description" content="Contact information for the USF Advanced Visualization Center.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="<?php echo $root ?>/about/">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation dont-collapse-nav default-body max-width">
      <?php include "template/menu.php";?>
      <article>
        <h2>Contact</h2>
        <h3>3D Printing</h3>
        <p class="no-justify">To submit 3D printing files or for any other information about 3D printing, email <a href="mailto:advancedvisualizationcenter@gmail.com" target="_blank">AdvancedVisualizationCenter@gmail.com</a>.</p>
        <p>For all other 3D printing-related inquiries, contact Gilberto Jaimes:</p>
        <address class="indented" id="gjaimes" itemscope itemtype="http://schema.org/Person" itemprop="employee">
          <h4 itemprop="name">Giberto Jaimes</h4>
          <span itemprop="jobTitle">3D Print Lab Manager, Technology & Systems Analyst</span><br>
          <a href="mailto:gjaimes@mail.usf.edu" target="_blank" itemprop="email">gjaimes@mail.usf.edu</a><br>
          <a href="tel:18139742037" target="_blank" itemprop="telephone" content="+18139742037">+1 (813) 974-2037</a>
        </address>
        <h3>Other Inquiries</h3>
        <p>For any other inquiries, contact Howard Kaplan:</p>
        <address class="indented" id="hkaplan" itemscope itemtype="http://schema.org/Person" itemprop="employee">
          <h4 itemprop="name">Howard Kaplan</h4>
          <span itemprop="jobTitle">Advanced Technologies Manager</span><br>
          <a href="mailto:howardkaplan@usf.edu" target="_blank" itemprop="email">howardkaplan@usf.edu</a><br> 
        </address>
        <h3>Website Issues</h3>
        <p class="no-justify">Found a problem with this website? Please <a href="https://github.com/usf-avc/public-website/issues/" target="_blank">submit it here</a>.</p>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>
</body>
</html>
