<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Our Staff | About | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Staff Information">
  <meta name="og:description" content="Information about current employees at the Advanced Visualization Center at USF.">
  <meta name="description" content="Information about current employees at the Advanced Visualization Center at USF.">
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="<?php echo $root ?>/about/staff.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <article class="space-paragraphs">
        <h2>Our Staff</h2>
        <section>
          <h3>Primary Staff</h3>
          <section itemprop="employee" itemscope itemtype="http://schema.org/Person">
            <h4 id="hkaplan"><span itemprop="name">Howard Kaplan</span>: <span itemprop="jobTitle">Advanced Technologies Manager</span></h4>
            <p><a href="about#hkaplan" itemprop="url">Contact Howard Kaplan</a></p>
            <div itemprop="description">
              <p>Howard uses multiple types of visualization as a means of study and application; including its use as an educational tool for learning in the context of <abbr title="Science, Technology, Engineering, Art, and Mathematics">STEAM</abbr>. Many of his visualization applications revolve around real-world data, 3D graphics and simulation, and 2d interactive media. He often uses instructional design techniques and graphics programming to provide students and educators with technology that fit with their class projects, course presentations and research needs. He received a BFA from <a href="https://www.ringling.edu/" itemprop="alumniOf">Ringling College of Art</a>, and an M.Ed from the University of South Florida. He is currently pursuing a Ph.D, in Engineering Science, Biomedical and Chemical Engineering at the University of South Florida.</p>
              <p>His unique skillset has allowed him to develop the Advanced Visualization Center, which serves as a campus wide resource. He has since also implemented the <a href="resources/3d_printing.php">3D Printing Lab</a>, and <a href="resources/rooms.php#computerlab">Visualization Computer Lab</a>. Howard's work has also been featured in the journal Science, Wired.com, Siggraph, Discovery.com, and Fox News. He was also selected by the Center for Digital Education as a <a href="http://www.centerdigitaled.com/paper/CDE-Top-30-2014.html">Top 30 Technologists, Transformers and Trailblazers in 2014</a>.</p>
              <p>Areas include: 2D & 3D Imaging, Medical Simulation, 3D Printing, Tactile Visualization, Physicalization, User Experience Design (UX), Instructional Design, Graphic User Interface Design (GUI), High Resolution Displays, Virtual Reality, Augmented Reality, Stereoscopic Technologies, gesture control, creative coding, rapid prototyping, Game Design, 3D Modeling, Animation, and alternative methods for data representation.</p>
            </div>
          </section>
          <section itemprop="employee" itemscope itemtype="http://schema.org/Person">
            <h4 id="gjaimes"><span itemprop="name">Gilberto Jaimes</span>: <span itemprop="jobTitle">Technology & Systems Analyst</span></h4>
            <p><a href="about#gjaimes" itemprop="url">Contact Gilberto Jaimes</a></p>
            <p itemprop="description">Gilberto researches visualization techniques in vast areas of 3d design and modeling to create interactive 3D Simulations, 3D models, and 3D prints for students, researchers and staff. Brought in as an undergraduate student, Gilberto evolved his skillset to include areas of data analysis, statistics, and scripting while completing a BS in Biomedical Sciences and an MS in Bioinformatics and Computational Biology. His area of expertise resides in 3D Modeling and Animation using the software packages such as Maya, Inventor, Meshmixer, Alias Autostudio, and Blender. Using these skills he's created 3D simulations for with students for class projects, taught workshops on 3D Printing and 3D modeling, and has created customized 3D models for researchers. He has also incorporated scripting in the form of Python and R for statistical Analyses and continues to develop his skills in web development using HTML, CSS, JS, PHP, Laravel</p>
          </section>
          <section itemprop="employee" itemscope itemtype="http://schema.org/Person">
            <h4 id="smason"><span itemprop="name">Spenser Mason</span>: <span itemprop="jobTitle">Software Developer</span></h4>
            <p itemprop="description">Spenser is a computer programmer with a BS in Computer Science from the University of South Florida. He is experienced in a wide range of software development fields, including web scraping, data analysis, game development, educational software, front-end web development, etc. He specializes in the Unity game engine to develop software as well as JavaScript and webpage development. His tasks there have ranged from teaching others how to program so they can write their own software, to developing a full-scale educational app for mobile devices using augmented reality and 3D printing. His typical projects involve visualizations of data using platforms such as the d3 JavaScript library. He also frequently performs other computer-related tasks such as administrative work on the computers in the <a href="resources/rooms.php#computerlab">Visualization Computer Lab</a>.</p>
          </section>
        </section>
        <!-- <section>
          <h3>Student Assistants</h3>
          <section itemprop="employee" itemscope itemtype="http://schema.org/Person">
            <h4 id="isanders"><span itemprop="name">Ian Sanders</span>: <span itemprop="jobTitle">Software Development Assistant</span></h4>
            <p itemprop="description">Ian is an undergraduate Mechanical Engineering student with a minor in Entrepreneurship. He has extensive experience developing with modern web technologies such as Node.js, HTML5, CSS3, and browser JavaScript. Ian also has experience with Python, MATLAB and PHP. His projects at the AVC include developing a dynamic display kiosk, building an inventory management system with Google Scripts and Google Sheets, and building this website. Ian is also the Vice President at the <a href="http://usfsoar.com/">USF Society of Aeronautics and Rocketry</a>.</p>
          <section>
        </section> -->
        <section>
          <h3>Past Employees</h3>
          <section>
            <h4 itemprop="alumni" itemscope itemtype="http://schema.org/Person">
              <span itemprop="name">Javier De La Vega</span>: <span itemprop="jobTitle">Visualization Lab Monitor</span>
            </h4>
            <p itemprop="description">Javier is currently working as a data analyst supporting federal workers' compensation programs.</p>
          </section>
          <section>
            <h4 itemprop="alumni" itemscope itemtype="http://schema.org/Person">
              <span itemprop="name">Harish Kumar Chittam</span>: <span itemprop="jobTitle">Visualization Lab Monitor</span>
            </h4>
            <p itemprop="description">Harish is currently working as a senior design assurance engineer at Danaher Corporation.</p>
          </section>
          <section itemprop="alumni" itemscope itemtype="http://schema.org/Person">
            <h4><span itemprop="name">Matthew Wedebrock</span>: <span itemprop="jobTitle">Software Development Assistant</span></h4>
            <p itemprop="description">Matthew Wedebrock is currently employed at Honeywell Aerospace.</p>
          </section>
          <section itemprop="alumni" itemscope itemtype="http://schema.org/Person">
            <h4><span itemprop="name">Ian Sanders</span>: <span itemprop="jobTitle">Software Development Assistant</span></h4>
            <p itemprop="description">Ian is currently working as an intern at Chromalloy Castings and is the Vice President at the <a href="http://usfsoar.com/">USF Society of Aeronautics and Rocketry</a>.</p>
          </section>
        </section>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>
</body>
</html>
