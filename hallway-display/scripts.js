/**
 * Convert 24-hour time integer into 12-hour time string.
 * @param {number} time - Integer representing a 24-hour (military) time value.
 * @example
 * // returns "1:45 PM"
 * parse24HourTime(1345);
 * @returns {string} A
 */
function parse24HourTime(time) {
  let minutes = time % 100,
      hours = Math.floor(time / 100) % 24;

  if(minutes < 10) {
    minutes = "0" + minutes;
  } else if(minutes > 59) {
    minutes = minutes - 59;
    hours++;
  }

  if(hours > 12) {
    return `${hours - 12}:${minutes} PM`;
  } else if(hours == 0) {
    return `12:${minutes} AM`;
  } else {
    return `${hours}:${minutes} AM`;
  }
}

/**
 * Sanitizes a string and replaces newline chracters with new paragraphs.
 * @param {string} input - Text to be parsed, with "/n" to represent new line.
 * @returns - HTML string.
 */
function parseParagraphText(input) {
  // Sanitizes input and replaces "/n" with "</p><p>"
  return '<p>' + input.replace(/&/g, '&amp;')
                      .replace(/</g, '&lt;')
                      .replace(/"/g, '&quot;')
                      .replace(/\\n/g, '</p><p>') + '</p>';
}

/**
 * After 1 minute from page load, check and make sure the google api 
 * authorized. If not, reload.
 */
setTimeout(function() {
  try {
    if(!gapi.auth2.getAuthInstance().isSignedIn.get()) {
      throw Error('Google API didn\'t authorize.');
    } else {
      console.log('Looks like the Google API loaded succesfully.');
    }
  } catch(e) {
    window.location.reload(true);
  }
}, 60*1000);

/**
 * Callback to be run after Google API auth.
 */
function runAfterGapiAuth() {
  try {
    // Boilerplate:
    const opts = {
      optionsSheetID: "16Pjdxt1MPlnP9l6xkTouc51NEq47uf1uzDZav_6uNtQ",
      queueSheetID: "1FP2gAWmtRNhtJtldWMV2uoWq2N7hAfXMRi5a3RqXEOc",
      updateOptionsInterval: 5*60*1000,
      reloadPageInterval: 3*60, // Minutes
      slideDuration: -1,
      calendarSlideNumber: 4,
      gallerySlideNumber: 2,
      slideTransitionDuration: 250, 
        // Don't forget to change the .slide {} CSS transition duration in styles.css
      numSlides: 8,
      printing: {
        labOpen: -1,
        labClose: -1,
        numPrinters: -1,
        printPrice: -1,
        minPrintCost: -1,
        updateQueueInterval: -1,
        printingStatus: "",
        queuedStatus: "",
        hoursColumn: -1,
        minutesColumn: -1,
        image: "",
        sheetRange: ""
      },
      announcements: [{
        title: "",
        text: "",
        image: ""
      }],
      featuredPub: {
        file: "",
        url: ""
      },
      accolades: {
        main: {
          title: "",
          text: "",
          images: ["",""]
        },
        side: [{
            title: "",
            text: "",
            image: ""
          }, {
            title: "",
            text: "",
            image: ""
          }
        ]
      },
      featuredResearcher: {
        name: "",
        images: ["",""]
      },
      galleryImages: [""],
      fetched: false
    },
    // All elements to be used:
    contElements = {
      printing: {
        labClose:           document.getElementById("labClose"),
        openIndicator:      document.getElementById("openClosed"),
        labOpen:            document.getElementById("labOpen"),
        openIndicatorText:  document.getElementById("openStatus"),
        numPrinters:        document.getElementById("numPrinters"),
        printPrice:         document.getElementById("printPrice"),
        printerDots:        document.getElementById("printerDots"),
        minPrintCost:       document.getElementById("minPrintCost"),
        queuedPrints:       document.getElementById("queuedPrints"),
        printingPrints:     document.getElementById("printingPrints"),
        totalPrints:        document.getElementById("totalPrints"),
        queueHours:         document.getElementById("queueHours"),
        queueDays:          document.getElementById("queueDays"),
        printImage:         document.getElementById("printingGallery"),
      },
      announcements: Array.from(document.getElementsByClassName("announcements")),
      featuredPub: {
        file: document.getElementById("featuredPublicationPDF"),
        url:  document.getElementById("featuredPublicationURL")
      },
      accolades: {
        main: {
          title: document.getElementById("mainArticleTitle"),
          images: [
            document.getElementById("mainArticleImage-0"),
            document.getElementById("mainArticleImage-1")
          ],
          text:  document.getElementById("mainArticleText")
        },
        side: [{
            title:  document.getElementById("sideArticle-0Title"),
            image: document.getElementById("sideArticle-0Image"),
            text:  document.getElementById("sideArticle-0Text")
          }, {
            title:  document.getElementById("sideArticle-1Title"),
            image: document.getElementById("sideArticle-1Image"),
            text:  document.getElementById("sideArticle-1Text")
          }
        ]
      },
      gallery:      document.getElementById("gallery"),
      weekCalendar: document.getElementById("weekCalendarEmbed"),
      featuredResearcher: {
        name: document.getElementById("featuredResearcherTitle"),
        images: [
          document.getElementById("featuredResearcherImg-0"),
          document.getElementById("featuredResearcherImg-1")
        ]
      }
    };

    let pageName = document.getElementById("pageName"),
        currentSlide = 7,
        firstRun = true;

    /**
     * Recursive slide changing function that calls itself with setTimeout.
     * (SetInterval would get too inaccurate over time and can cause other issues)
     */
    function changeSlide() {
      // Start slideshow over when end is reached
      currentSlide < opts.numSlides - 1 ? currentSlide++ : currentSlide = 0;

      let slideItems = Array.from(document.getElementsByClassName("slide")),
          hidePageName = false;

      // To pull off the fade transition, we fade everything out and then fade 
      // everything back in
      slideItems.forEach(element => element.classList.add("faded-out"));
      pageName.classList.add("faded-out");

      setTimeout(function() {
        slideItems.forEach(element => {
          if(element.classList.contains(`slide-${currentSlide}`)) {
            element.classList.remove("not-current-slide");
            if(element.classList.contains("hide-pagename")) hidePageName = true;
          } else {
            element.classList.add("not-current-slide");
          }
        });

        if(hidePageName) {
          pageName.classList.add("not-current-slide");
        } else {
          pageName.classList.remove("not-current-slide");
        }

        // When we hide and reshow the iframe, it scrolls up to the top, but 
        // since it's a schedule we want to have it scrolled to the default 
        // location, so we have to reload it every damn time we show it
        if(currentSlide == opts.calendarSlideNumber) {
          contElements.weekCalendar.src += "";
        }

        setTimeout(function() {
          slideItems.forEach(element => element.classList.remove("faded-out"));
          pageName.classList.remove("faded-out");
        }, 150);
      }, opts.slideTransitionDuration);

      // Change gallery image when slide after gallery is showing
      if(currentSlide == opts.gallerySlideNumber + 1 || firstRun) {
        let randImageIndex = Math.floor(Math.random() * opts.galleryImages.length);
        contElements.gallery.style.backgroundImage = 
          `url("${opts.galleryImages[randImageIndex]}")`;

        firstRun = false;
      }

      setTimeout(changeSlide, opts.slideDuration);
    }

    /**
     * Recursive info updating function that calls itself with setTimeout.
     * (SetInterval would get too inaccurate over time and can cause other issues)
     */
    function updateQueueInfo() {
      if(gapi.auth.authorize) {
        gapi.client.sheets.spreadsheets.values.get({
          spreadsheetId: opts.queueSheetID,
          range: opts.printing.sheetRange
        }).then(response => {
          let values = response.result.values;

          let jobs = {
            printing: 0,
            queued: 0,
            totalMinutes: 0,
            total: 0
          }

          for(let i = 2; i < values.length; i++) {
            let row = values[i],
                addTime = true;

            if(row[0] == opts.printing.printingStatus) {
              jobs.printing++;
            } else if(row[0] == opts.printing.queuedStatus) {
              jobs.queued++;
            } else if(row[0]) {
              jobs.total++; // Total completed jobs this season
              addTime = false;
            } else {
              addTime = false;
            }

            if(addTime) {
              let hours = +row[opts.printing.hoursColumn],
                  minutes = +row[opts.printing.minutesColumn];

              if(!isNaN(hours)) {
                jobs.totalMinutes += hours * 60;
              } else {
                console.warn(`Invalid value for hours in row ${i + 1}!`);
              }

              if(!isNaN(minutes)) {
                jobs.totalMinutes += minutes;
              } else {
                console.warn(`Invalid value for minutes in row ${i + 1}!`);
              }
            }
          };

          contElements.printing.queuedPrints.textContent = jobs.queued;
          contElements.printing.printingPrints.textContent = jobs.printing;
          contElements.printing.totalPrints.textContent = jobs.total;
          contElements.printing.queueHours.textContent = Math.ceil((jobs.totalMinutes % (60 * 24)) / 60);
          contElements.printing.queueDays.textContent = Math.floor(jobs.totalMinutes / (60 * 24));
        });
      } else initClient();
      setTimeout(updateQueueInfo, opts.printing.updateQueueInterval);
    }

    /**
     * Recursive status updating function that calls itself with setTimeout.
     * (SetInterval would get too inaccurate over time and can cause other issues)
     * Also reloads the page every so often.
     */
    let minutesSinceLastReload = 0;
    function updateOpenIndicator() {
      let currentDate = new Date();
      currentTime = currentDate.getHours() * 100 + currentDate.getMinutes();
      if(currentTime >= opts.printing.labOpen && currentTime <= opts.printing.labClose) {
        contElements.printing.openIndicator.classList.remove("closed");
        contElements.printing.openIndicatorText.textContent = "open";
      } else {
        contElements.printing.openIndicator.classList.add("closed");
        contElements.printing.openIndicatorText.textContent = "closed";
      }
      
      if(minutesSinceLastReload >= opts.reloadPageInterval) {
        location.reload(true);
      } else {
        minutesSinceLastReload++;
      }
      setTimeout(updateOpenIndicator, 60*1000);
    }

    /**
     * Recursive IIFE updating function that calls itself with setTimeout.
     * (SetInterval would get too inaccurate over time and can cause other issues)
     * Also calls the other functions.
     */
    (function updateOptions() {
      if(gapi.auth.authorize) {
        gapi.client.sheets.spreadsheets.values.get({
          spreadsheetId: opts.optionsSheetID,
          range: "options"
        }).then(response => {
          /// Pull options from configurator spreadsheet
          let values = response.result.values;

          // Slide Duration
          opts.slideDuration = values[1][1] * 1000;

          // 3D Printing Information section
          opts.printing.labOpen              = +values[ 4][1];
          opts.printing.labClose             = +values[ 5][1];
          opts.printing.numPrinters          = +values[ 6][1];
          opts.printing.printPrice           =  values[ 7][1];
          opts.printing.minPrintCost         =  values[ 8][1];
          opts.printing.updateQueueInterval  = +values[ 9][1] * 1000;
          opts.printing.printingStatus       =  values[10][1];
          opts.printing.queuedStatus         =  values[11][1];
          opts.printing.hoursColumn          = +values[12][1];
          opts.printing.minutesColumn        = +values[13][1];
          opts.printing.sheetRange           =  values[14][1];
          opts.printing.image                =  values[15][1];

          // Announcements section
          for(let i = 0; i < 5; i++) {
            let rowNum = i + 19;
            if(values[rowNum] && values[rowNum][0]) {
              opts.announcements[i] = {
                title: values[rowNum][0],
                text:  parseParagraphText(values[rowNum][1]),
                // Show a transparent image to avoid 'broken image' icon
                image: values[rowNum][2].trim() || 
                  "https://upload.wikimedia.org/wikipedia/commons/c/ce/Transparent.gif"
              }
            }
          }

          // Featured Publication section
          let oldPDF = opts.featuredPub.file;
          opts.featuredPub.url = values[27][0];
          opts.featuredPub.file = values[27][1];

          // Accolades section
          opts.accolades.main.title = values[31][1];
          opts.accolades.main.text  = parseParagraphText(values[31][2]);
          opts.accolades.main.images = [values[31][3], values[31][4]];
          for(let i = 0; i < 2; i++) {
            let rowNum = i + 32;
            opts.accolades.side[i].title = values[rowNum][1];
            opts.accolades.side[i].text  = parseParagraphText(values[rowNum][2]);
            opts.accolades.side[i].image = values[rowNum][3];
          }

          // Featured Researcher section
          opts.featuredResearcher.name = values[37][0];
          opts.featuredResearcher.images[0] = values[37][1];
          opts.featuredResearcher.images[1] = values[37][2];

          // Slideshow Images (gallery) section
          for(let i = 0; i < 100; i++) {
            let rowNum = i + 40;
            if(values[rowNum] && values[rowNum][0]) {
              opts.galleryImages[i] = values[rowNum][0];
            }
          }

          // only should run the first time the options are loaded
          if(!opts.fetched) {
            // These run as separate functions because each runs recursively 
            // on a different interval
            changeSlide();
            updateQueueInfo();
            updateOpenIndicator();
          }

          // only should run when the featured PDF changes
          if(oldPDF !== opts.featuredPub.file) {
            updateFeaturedPDF();
          }

          if(values) opts.fetched = new Date();
          
          // Options are updated, now update actual content:
          // Lab hours:
          contElements.printing.labOpen.textContent = 
            parse24HourTime(opts.printing.labOpen);
          contElements.printing.labClose.textContent = 
            parse24HourTime(opts.printing.labClose);
          // Number of printers:
          contElements.printing.numPrinters.textContent = 
            opts.printing.numPrinters;
          contElements.printing.printerDots.textContent = 
            "● ".repeat(opts.printing.numPrinters).trim();
          // Image:
          contElements.printing.printImage.style.backgroundImage = 
            `url("${opts.printing.image}")`;
          // Price:
          contElements.printing.printPrice.textContent = 
            opts.printing.printPrice;
          contElements.printing.minPrintCost.textContent = 
            opts.printing.minPrintCost;
          // Announcements:
          opts.announcements.forEach((anncmnt, index) => {
            // It's better to build the HTML from scratch then leave 5 empty 
            // announcements and edit them
            let anncmntContent = 
              `<h3><span>${anncmnt.title}</span></h3>
                ${anncmnt.text}<img src="${anncmnt.image}"/>`;
            contElements.announcements[index].innerHTML = anncmntContent;
          });
          // Featured Publication URL:
          contElements.featuredPub.url.textContent = opts.featuredPub.url;
          // Accolades:
          contElements.accolades.main.title.textContent    = opts.accolades.main.title;
          contElements.accolades.main.text.innerHTML       = opts.accolades.main.text;
          contElements.accolades.main.images[0].src = opts.accolades.main.images[0];
          contElements.accolades.main.images[1].src = opts.accolades.main.images[1];
          contElements.accolades.side[0].title.textContent = opts.accolades.side[0].title;
          contElements.accolades.side[0].text.innerHTML = opts.accolades.side[0].text;
          contElements.accolades.side[0].image.src  = opts.accolades.side[0].image;
          contElements.accolades.side[1].title.textContent = opts.accolades.side[1].title;
          contElements.accolades.side[1].text.innerHTML    = opts.accolades.side[1].text;
          contElements.accolades.side[1].image.src  = opts.accolades.side[1].image;
          // Featured Researcher:
          contElements.featuredResearcher.name.textContent = opts.featuredResearcher.name;
          contElements.featuredResearcher.images[0].style.backgroundImage = 
            `url(${opts.featuredResearcher.images[0]})`;
          contElements.featuredResearcher.images[1].style.backgroundImage = 
            `url(${opts.featuredResearcher.images[1]})`;
        });
      } else initClient();

      setTimeout(updateOptions, opts.updateOptionsInterval);
    })();

    // Because loading a PDF is intensive, we only call it when the file 
    // option has changed, so it's not recursive
    function updateFeaturedPDF() {
      let loadingTask = PDFJS.getDocument(opts.featuredPub.file);
      loadingTask.promise.then(function(pdf) {
        
        // Fetch the first page
        pdf.getPage(1).then(function(page) {
          
          var viewport = page.getViewport(1.5);

          // Prepare canvas using PDF page dimensions
          let canvas = contElements.featuredPub.file,
              context = canvas.getContext('2d');
          canvas.height = viewport.height;
          canvas.width = viewport.width;

          // Render PDF page into canvas context
          let renderContext = {
            canvasContext: context,
            viewport: viewport
          };
          let renderTask = page.render(renderContext);
          renderTask.then(function () {
          });
        });
      }, function (reason) {
        // PDF loading error
        console.error(reason);
      });
    }
  } catch(e) {
    // On error, reload after a delay
    setTimeout(function() {
      window.location.reload(true);
    }, 20*1000);
  }
}
