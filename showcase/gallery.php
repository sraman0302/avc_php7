<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Gallery | Showcase | USF AVC</title>

  <?php include "../template/galleryThumbFrom.php";?>

  <meta name="og:title" content="USF Advanced Visualization Center - Gallery">
  <meta name="og:description" content="These images and videos represent the work and capabilities of the Advanced Visualization Center.">
  <meta name="description" content="These images and videos represent the work and capabilities of the Advanced Visualization Center.">
  <meta name="og:image" content="media/3D_printing/golfing_prosthetic.jpg">

  <meta name="og:image:alt" content="Closeup view of a 3D printed prosthetic that will enable a veteran with missing fingers to play golf.">
  <meta name="og:url" content="<?php echo $root ?>/showcase/gallery.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <article>
        <h2>Image Gallery</h2>
        <p>The following images and videos represent the work and capabilities of the Advanced Visualization Center.</p>

        <section class="table-of-contents">
          <h3 class="accent-header"><span>Contents</span></h3>
          <ul class="bullet-list">
            <li><a href="showcase/gallery.php#research">Research</a></li>
            <li><a href="showcase/gallery.php#printing">3D Printing</a></li>
            <li><a href="showcase/gallery.php#xr">Virtual & Augmented Reality</a></li>
            <li><a href="showcase/gallery.php#vis-wall">Visualization Wall</a></li>
            <li><a href="showcase/gallery.php#computer-lab">Visualization Computer Lab</a></li>
          </ul>
        </section>

        <section class="image-row">
          <h3 id="research">Research</h3>
          
          <div class="image-row-container" data-featherlight-gallery data-featherlight-filter="a">
            <div>
              <?php galleryThumbFrom("showcase/media/research/ACL_injury.jpg",
                "A young USF Stem Scholar gives a presentation on ACL (Advanced Crcuaite Ligament) injuries, with the aid of a 3D printed model.",
                "../../showcase/media/research/ACL_injury.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/ar_cubes.jpg",
                "An Android app, developed with the help of the AVC, enables users to scan 3D printed cubes and show related information (about green energy) on a tablet.",
                "../../showcase/media/research/ar_cubes.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/coax.jpg",
                "Student-created 3D model of the workings of a coaxial cable, developed with AVC assistance.",
                "../../showcase/media/research/coax.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/deformation.jpg",
                "A 3D structural analysis shows deformation caused by a heavy ball resting on a flexible lattice.",
                "../../showcase/media/research/deformation.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/ear.jpg",
                "A 3D scanned and printed human ear, developed using AVC resources.",
                "../../showcase/media/research/ear.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/flow.jpg",
                "Visualization of electromagnetic field in 3D space.",
                "../../showcase/media/research/flow.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/heart_lungs.jpg",
                "Simplified 3D model of human heart and lungs.",
                "../../showcase/media/research/heart_lungs.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/molecule.jpg",
                "3D model of complex organic molecule and its electrons.",
                "../../showcase/media/research/molecule.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/resistivity.jpg",
                "Visualization of resistivity in a cave system.",
                "../../showcase/media/research/resistivity.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/research/wave.jpg",
                "3D model of waves interacting with particles in a medium.",
                "../../showcase/media/research/wave.jpg") ?>
            </div>
          </div>
        </section>

        <section class="image-row">
          <!-- ID can't start with a number so it can't be "3D-printing" -->
          <h3 id="printing">3D Printing</h3>
          
          <div class="image-row-container" data-featherlight-gallery data-featherlight-filter="a">
            <div>
              <?php galleryThumbFrom("media/3D_printing/golfing_prosthetic.jpg",
                "Closeup view of a 3D printed prosthetic that will enable a veteran with missing fingers to play golf.",
                "../../media/3D_printing/golfing_prosthetic.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/golfing_prosthetic_howard.jpg",
                "Visualization Specialist Howard Kaplan demonstrates usage of a prosthetic golf club holder to a veteran at the VA Hospital.",
                "../../media/3D_printing/golfing_prosthetic_howard.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/3d_printers.jpg",
                "A small sample of the 3D Printers available in our printing lab.",
                "../../media/3D_printing/3d_printers.jpg") ?>
            </div>
            <div>
              <a href="media/3D_printing/timelapse.mp4" class="video-thumb" data-featherlight="<video src='media/3D_printing/timelapse.mp4' autoplay><a title='Video timelapse of 3d printing process of a large skull (no audio).' href='media/3D_printing/timelapse.mp4'></a></video>">
                <?php echo'<img alt="Video timelapse of 3D printing a large skull." src="'.htmlspecialchars(phpThumbURL('src=../../media/3D_printing/timelapse_thumb.jpg&w=200&h=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'">';?>
              </a>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/printed_dinosaur.jpg",
                "High-quality small blue 3D printed dinosaur head, highlighting detailed printing capabilities.",
                "../../media/3D_printing/printed_dinosaur.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/printed_heart.jpg",
                "Two white printed anatomical hearts, showing scaling capabilites.",
                "../../media/3D_printing/printed_heart.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/printed_people.jpg",
                "Several miniature printed busts, showing 3D scanning and detailed printing.",
                "../../media/3D_printing/printed_people.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/resin_bug.jpg",
                "A small transparent bug, printed with a resin process that makes it perfectly smooth.",
                "../../media/3D_printing/resin_bug.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/printed_spike_makerbot.jpg",
                "One of our Makerbot Replicator 2 printers printes a translucent model of a droplet of liquid making waves.",
                "../../media/3D_printing/printed_spike_makerbot.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/prosthetic_hand_2.jpg",
                "Gray and turquoise printed prosthetic hand.",
                "../../media/3D_printing/prosthetic_hand_2.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/prosthetic_hand.jpg",
                "Brown and white 3D printed prosthetic hand model.",
                "../../media/3D_printing/prosthetic_hand.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/variety.jpg",
                "Collection of models printed by the AVC in the 3D Printing Lab.",
                "../../media/3D_printing/variety.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/vases.jpg",
                "Collection of black and white artistic geometric vases printed by the AVC.",
                "../../media/3D_printing/vases.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/shark.jpg",
                "Shark robot created by Christopher Comando, a 14 year-old student.",
                "../../media/3D_printing/shark.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/genshaft_present.jpg",
                "A student presents her 3D Printed creation to USF President Dr. Judy Genshaft in the AVC Auditorium.",
                "../../media/3D_printing/genshaft_present.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("media/3D_printing/genshaft_present2.jpg",
                "Another student presents her 3D Printed project to Dr. Genshaft in front of the Visualization Wall.",
                "../../media/3D_printing/genshaft_present2.jpg") ?>
            </div>
          </div>
        </section>
        
        <section class="image-row">
          <h3 id="vis-wall">Visualization Wall</h3>
          
          <div class="image-row-container" data-featherlight-gallery data-featherlight-filter="a">
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/3d_presentation.jpg",
                "A student presents a stereoscopic 3D visualization for her class project.",
                "../../showcase/media/visualization_wall/3d_presentation.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/dino_model_wall.jpg",
                "A 3D mesh model of a dinosaur skull is shown on the Visualization Wall.",
                "../../showcase/media/visualization_wall/dino_model_wall.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/dinosaur_drawing.jpg",
                "A professor discusses a sketch of an ancient animal shown on the display.",
                "../../showcase/media/visualization_wall/dinosaur_drawing.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/kaplan.jpg",
                "Howard Kaplan (director of the AVC) and his doctoral thesis advisor stand in front of the Visualization Wall.",
                "../../showcase/media/visualization_wall/kaplan.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/mybrew.jpg",
                "Students present their business plan for a company called MyBrew using slides shown on the Visualization Wall.",
                "../../showcase/media/visualization_wall/mybrew.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/archeology.jpg",
                "Large-format archeological photographs are spread accross the floor and displayed on the visualization wall.",
                "../../showcase/media/visualization_wall/archeology.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/religion.jpg",
                "A student conducts a presentation on a religious topic, using the Visualization Wall as an aid.",
                "../../showcase/media/visualization_wall/religion.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/biophysics.jpg",
                "A student presents about a biophysics topic.",
                "../../showcase/media/visualization_wall/biophysics.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/secret_lab.jpg",
                "A student team presents their business plan for Secret Lab, using a physical model as well as the display wall.",
                "../../showcase/media/visualization_wall/secret_lab.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/video_chat.jpg",
                "Student videoconference with a remote expert in the Advanced Visualization Auditorium.",
                "../../showcase/media/visualization_wall/video_chat.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/auditorium.jpg",
                "University President Judy Genshaft watches as a student presents a new app at the AVC Auditorium.",
                "../../showcase/media/visualization_wall/auditorium.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/genshaft_presentation.jpg",
                "A student presents her project to Dr. Genshaft in the Visualization Auditorium.",
                "../../showcase/media/visualization_wall/genshaft_presentation.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/presentation.jpg",
                "Several USF faculty and administration view a presentation on the Visualization Wall.",
                "../../showcase/media/visualization_wall/presentation.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/visualization_wall/map.jpg",
                "Dr. Rya Carney, PhD. presents a topology visualization using the AVC Visualization Wall.",
                "../../showcase/media/visualization_wall/map.jpg") ?>
            </div>
          </div>
        </section>

        <section class="image-row">
          <h3 id="xr">Virtual & Augmented Reality</h3>
          
          <div class="image-row-container" data-featherlight-gallery data-featherlight-filter="a">
            <div>
              <?php galleryThumbFrom("showcase/media/xr/genshaft_holo.jpg",
                "USF System President, Dr. Judy Genshaft, tries out a Hololens at the Advanced Visualization Center.",
                "../../showcase/media/xr/genshaft_holo.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/xr/carney.jpg",
                "Dr. Ryan Carney, PhD, uses a Microsoft Hololens augmented reality device to present a model.",
                "../../showcase/media/xr/carney.jpg") ?>
            </div>
          </div>
        </section>

        <section class="image-row">
          <h3 id="computer-lab">Computer Lab</h3>
          
          <div class="image-row-container" data-featherlight-gallery data-featherlight-filter="a">
            <div>
              <?php galleryThumbFrom("showcase/media/computer_lab/forensics.jpg",
                "Students engage in a forensics class in the Visualization Computer Lab by following along on their screens.",
                "../../showcase/media/computer_lab/forensics.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/computer_lab/students.jpg",
                "Students collaborate on a visualization project using the high-performance computers in the Visualization Computer Lab.",
                "../../showcase/media/computer_lab/students.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/computer_lab/teacher.jpg",
                "A professor shows a group of students a detail on a 3D model on one of the computers.",
                "../../showcase/media/computer_lab/teacher.jpg") ?>
            </div>
          </div>
        </section>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>

  <script src="libraries/jquery-3.3.1.min.js"></script>
  <script src="libraries/featherlight/release/featherlight.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.min.css">
  <script src="libraries/featherlight/release/featherlight.gallery.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.gallery.min.css">
  <script src="scripts/featherlight_config.js"></script>
</body>
</html>
