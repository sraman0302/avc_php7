<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Showcase | USF AVC</title>
     <base href="http://127.0.0.1:8080/edsa-AVC%201/">
    <link rel="stylesheet" href="template/resets.css">
  <link rel="stylesheet" href="template/template.css">
  <link rel="stylesheet" href="template/widgets.css">
  <link rel="shortcut icon" type="image/ico" href="template/favicon.ico"/>
    
  <meta name="og:title" content="USF Advanced Visualization Center - Showcase">
  <meta name="og:description" content="The Advanced Visualization Center has worked on and provided consultation for a number of highly innovative, educational projects.">
  <meta name="description" content="The AVC has worked on and provided consultation for a number of highly innovative, educational projects.">
  <meta name="og:image" content="media/3D_printing/golfing_prosthetic.jpg">

  <meta name="og:image:alt" content="Closeup view of a 3D printed prosthetic that will enable a veteran with missing fingers to play golf.">
  <meta name="og:url" content="<?php echo $root ?>/showcase/">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation dont-collapse-nav default-body max-width">
      <?php include "template/menu.php";?>
      <article>
        <h2>Overview</h2>
        <p>The Advanced Visualization Center has worked on and provided consultation for a number of highly innovative, educational projects. See the menu links for more information.</p>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>
</body>
</html>
