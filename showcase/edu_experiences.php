<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>USF Advanced Visualization Center - Educational Experiences</title>

  <?php include "../template/galleryThumbFrom.php";?>

  <meta name="og:title" content="USF Advanced Visualization Center - Educational Experiences">
  <meta name="og:description" content="Educational experiences are part of the services offered by the Advanced Visualization Center under the department of Information Technology where technological resources are integrated with academics to provide students with unique opportunities. Collaborating with faculty and researchers, we help to develop these experiences throughout the year to supplement and enhance student pedagogy.">
  <meta name="description" content="Showcase of the educational experiences offered by the USF AVC.">
  <meta name="og:image" content="showcase/media/VA_Prosthetics/ImageB.jpg">

  <meta name="og:image:alt" content="Students watch a lecture in Dr. Anna Pryat's class.">
  <meta name="og:url" content="<?php echo $root ?>/showcase/edu_experiences.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <article>
        <h2>Educational Experiences</h2>
        <p>Educational experiences are part of the services offered by the Advanced Visualization Center under the department of Information Technology where technological resources are integrated with academics to provide students with unique opportunities. Collaborating with faculty and researchers, we help to develop these experiences throughout the year to supplement and enhance student pedagogy.</p>
        <section>
          <h3>Technology Integration With VA & Biomedical Engineering Students</h3>
          <small class="author">Dr. Anna Pryat: Assistant Professor, Chemical and Biomedical Engineering</small>
          <div class="aside_2-3">
            <p>Conducting classes at the Advanced Visualization Center not only provided amazing technical capabilities for my course, but also facilitated <a  href="about/staff.php#hkaplan">Howard Kaplan</a>, (AVC Manager) and I to build collaborations that provided students with unique opportunities. One of such collaborations involves my <cite>Modern Biomedical Technologies</cite> students. Through the relationships already established by AVC, the students are able to have much more authentic learning experiences working with the subject knowledge, technologies and medical professionals from the VA. During their semester-long projects we all work together to provide patients better quality of life by designing innovative supporting technologies restoring some of the abilities that they had previously. For example, two groups of students are currently working on the following devices: 1) Enabling a wounded veteran with amputated fingers to continue to play golf, and 2) Helping veteran to participate in wheelchair races.
            <figure class="quote">
              <blockquote>“I think that real world applications help the students to understand that their research actually has a purpose. It helps to create better engineers.”</blockquote> 
              <figcaption>Dr. Anna Pryat, PhD.</figcaption>
            </figure>
          </div>
          <hr>
          <figure class="quote">
            <blockquote>“The project we completed for Dr. Pryat's <cite>Modern Biomedical Technologies</cite> course provided us with professional opportunities that enhance our education in the biomedical engineering field.”</blockquote> 
            <figcaption>Rey Pilczuk, President, & Sara Redlick, Vice President; USF Biomedical Engineering Society.</figcaption>
          </figure>
        </section>
        <section class="image-row">
          <h3 class="accent-header"><span>Gallery</span></h3>
          <div class="image-row-container" data-featherlight-gallery data-featherlight-filter="a">
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageA.jpg",
                "3D printed black prosthetic foot brace.",
                "../../showcase/media/VA_Prosthetics/ImageA.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageB.jpg",
                "Students watch a lecture in Dr. Pryat's class.",
                "../../showcase/media/VA_Prosthetics/ImageB.jpg") ?>
            </div>        
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageC.jpg",
                "Several white 3D printed golfing-related prosthetics prototypes.",
                "../../showcase/media/VA_Prosthetics/ImageC.jpg") ?>
            </div>        
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageD.jpg",
                "Students view a lecture on modern prosthetic technologies in Dr. Pryat\'s class.",
                "../../showcase/media/VA_Prosthetics/ImageD.jpg") ?>
            </div>        
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageE.jpg",
                "Modern carbon fiber prosthetic leg piece.",
                "../../showcase/media/VA_Prosthetics/ImageE.jpg") ?>
            </div>        
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageF.jpg",
                "In a lecture in the Visualization Auditorium, students learn about a boy who 3D printed his own prosthetic hand.",
                "../../showcase/media/VA_Prosthetics/ImageF.jpg") ?>
            </div>        
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageG.jpg",
                "A patient at the VA hospital demonstrates how challenging it is to hold a golf club without prosthetics.",
                "../../showcase/media/VA_Prosthetics/ImageG.jpg") ?>
            </div>        
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageH.jpg",
                "A student in Dr. Pryat\'s class demonstrates a 3D printed hand he developed as a class project.",
                "../../showcase/media/VA_Prosthetics/ImageH.jpg") ?>
            </div>        
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageI.jpg",
                "Closeup of a 3D printed prosthetic hand.",
                "../../showcase/media/VA_Prosthetics/ImageI.jpg") ?>
            </div>        
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageJ.jpg",
                "Image of a VA patient\'s hand injury, in which the majority of his fingers are missing.",
                "../../showcase/media/VA_Prosthetics/ImageJ.jpg") ?>
            </div>
            <div>
              <?php galleryThumbFrom("showcase/media/VA_Prosthetics/ImageK.jpg",
                "Students in Dr. Pryat\'s class learn how a 3D printed prosthetic hand works.",
                "../../showcase/media/VA_Prosthetics/ImageK.jpg") ?>
            </div>
          </div>
        </section>
      </article>
    </div>
  </main>

  <?php include "../template/footer.php"?>

  <script src="libraries/jquery-3.3.1.min.js"></script>
  <script src="libraries/featherlight/release/featherlight.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.min.css">
  <script src="libraries/featherlight/release/featherlight.gallery.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.gallery.min.css">
  <script src="scripts/featherlight_config.js"></script>
</body>
</html>
