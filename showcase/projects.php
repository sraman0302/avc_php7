<!DOCTYPE html>
<html lang="en">
<head>
  <?php include "../template/head.php";?>

  <title>Projects | Showcase | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Projects">
  <meta name="og:description" content="The advanced visualization center has assisted USF faculty and staff in completing these research and development projects.">
  <meta name="description" content="The advanced visualization center has assisted USF faculty and staff in completing these research and development projects.">
  <meta name="og:image" content="showcase/media/projects/engbuilding.jpg">

  <meta name="og:image:alt" content="A 3D model of a proposed USF Engineering building is shown on an iPad in virtual AR space.">
  <meta name="og:url" content="<?php echo $root ?>/showcase/projects.php">
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "../template/header.php";?>
  <?php include "../template/menu.php";?>

  <main id="main-content">
    <?php include "template/header.php";?>
    <div class="has-navigation default-body max-width">
      <?php include "template/menu.php";?>
      <section class="space-paragraphs">
        <h2>Projects</h2>
        <p>The advanced visualization center has assisted USF faculty and staff in completing the following research and development projects:</p>

        <section class="table-of-contents">
          <h3 class="accent-header"><span>Contents</span></h3>
          <ul class="bullet-list">
            <li><a href="showcase/projects.php#arpa">ARPA</a></li>
            <li><a href="showcase/projects.php#fl-postcards">Florida Postcards</a></li>
            <li><a href="showcase/projects.php#engineering-bldg">AR Engineering Building Demonstration</a></li>
            <li><a href="showcase/projects.php#dozier">Dozier Fire Simulation</a></li>
            <li><a href="showcase/projects.php#vps">VPS</a></li>
            <li><a href="showcase/projects.php#archaeopteryx"><i lang="la">Archaeopteryx</i> HoloLens Viewer</a></li>
            <li><a href="showcase/projects.php#cop">Adrenergic Pharmacology VR</a></li>
          </ul>
        </section>

        <article>
          <h3 id="arpa"><abbr title="Augmented Reality Pedagogical Agent">ARPA</abbr></h3>
          <a href="showcase/media/projects/arpa.jpg" data-featherlight="image">
            <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../showcase/media/projects/arpa.jpg&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="The ARPA project uses colorful cubes in conjuction with image recognition to trigger content displays on a tablet." width="200">';?>
          </a>
          <p><abbr title="Augmented Reality">AR</abbr>-enhanced learning, was developed in collaboration with <a href="http://www.usf.edu/education/faculty/faculty-profiles/sanghoon-park.aspx">Dr. Sanghoon Park</a>, <a href="http://www.usf.edu/education/index.aspx">College of Education</a>, and is an innovative form of instructional technology that enables students to experience a real-time view of a physical world through augmented reality delivered digital learning content. The application was developed for both Android and iOS tablets.</p>
          <p>This project involved many different technologies, including:</p>
          <ol class="bullet-list">
            <li>3D modelling and 3D animation were used to create the three-dimensional avatar.</li>
            <li>3D printing was used to create the AR cubes.</li>
            <li><a href="https://unity3d.com/">Unity3D</a> was used to create the interactive experience and deploy to Android and iOS.</li>
          </ol>
        </article>

        <article>
          <h3 id="fl-postcards">Florida Postcards</h3>
          <a href="showcase/media/projects/flpostcards.jpg" data-featherlight="image">
            <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../showcase/media/projects/flpostcards.jpg&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="Screenshot of the Florida Postcards website." width="200">';?>
          </a>
          <p>The Florida Postcards project was initiated by <a href="http://english.usf.edu/faculty/lrgordon/">Laura Runge, PhD</a>. in order to visualize the world's largest Florida Postcard database, contained within the <a href="https://www.lib.usf.edu/">USF Library</a>.</p>
          <p>This project was written in HTML/CSS/Javascript and designed to run as a website. </p>
          <p>The visualizations created included:</p>
          <ol class="bullet-list">
            <li>A timeline of postcards</li>
            <li>An interactive map plotting the postcard sender and receiver coordinates</li>
            <li>An interactive dashboard with which users can explore the data in depth and search for specific cards</li>
          </ol>
        </article>

        <article>
          <h3 id="engineering-bldg">AR Engineering Building Demonstration</h3>
          <a href="showcase/media/projects/engbuilding.jpg" data-featherlight="image">
            <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../showcase/media/projects/engbuilding.jpg&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="A 3D model of the proposed building is shown on an iPad in virtual AR space." width="200">';?>
          </a>
          <p>The <abbr title="Augmented Reality">AR</abbr> Engineering Building demonstration is an AR-powered walk-through of a three story building proposed by the <a href="http://www.usf.edu/engineering/">USF Engineering</a> department. Using architectural renders, the Advanced Visualization Center created 3D models of the buildings exterior and interior. A custom interface and tracking system was developed to enable navigation of the building in the augmented / mixed reality space. Using a mobile tablet, the app allows users to view the building interactively from any angle, and features navigation buttons to display each floor, specific rooms, and the exterior environment.</p>
        </article>

        <article>
          <h3 id="dozier">Dozier Fire Simulation</h3>
          <a href="showcase/media/projects/dozier.jpg" data-featherlight="image">
            <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../showcase/media/projects/dozier.jpg&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="Top: Broken apart view of the school, showing various floors; Bottom: Simulation of the school building burning." width="200">';?>
          </a>
          <p>The Advanced Visualization Center, in Collaboration with <a href="http://anthropology.usf.edu/faculty/kimmerle/">Dr. Erin Kimmerle</a>, of the <a href="http://www.forensics.usf.edu/">USF Institute of Forensic Anthropology and Applied Sciences</a>, created a 3D simulation of the Dozier School for Boys fire that took place on <time datetime="1914-11-18">November 18, 1914</time>. The recreation was generated using 3D modeling and animation software. The digital models of the school were also 3D printed in sections so that viewers could see the layout more clearly. Other artifacts were also scanned, 3D printed, and shared virtually.</p>
          <section class="no-clear">
            <h4>Related Media</h4>
            <ul class="bullet-list">
              <li><a href="http://wusfnews.wusf.usf.edu/post/usf-conference-marks-100th-anniversary-dozier-school-fire#stream/0">WUSF News</a></li>
            </ul>
          </section>
        </article>
        
        <article>
          <h3 id="vps"><abbr title="Virtual Patient Safety">VPS</abbr></h3>
          <a href="showcase/media/projects/vps.jpg" data-featherlight="image">
            <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../showcase/media/projects/vps.jpg&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="3D simulation of a simplified hospital environment, with arrows overlayed on the floor." width="200">';?>
          </a>
          <p>The Virtual Patient Safety project is an educational Virtual Environment designed to train nursing staff on the proper procedures for preventing and responding to patient falls. Made in collaboration with the VA, the Advanced Visualization Center (AVC) designed and built 3D models including Avatars, virtual bots, the virtual environment and all of the assets. The AVC also generated the animations and avatar control systems. The simulation featured a full interactive 3D virtual environment in which the user plays the part of a nurse responding to a patient fall. An instructional system was developed to capture data about the each users interaction in the virtual space, as they answered multiple choice questions, input free text observations, and their time taken at each stage of the simulation. feedback was also given to the user after each response location.</p>
        </article>

        <article>
          <h3 id="archaeopteryx"><i lang="la">Archaeopteryx</i> HoloLens Viewer</h3>
          <a href="showcase/media/projects/archaeopteryx.jpg" data-featherlight="image">
            <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../showcase/media/projects/archaeopteryx.jpg&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="Dr. Carney demonstrates his Archeopteryx project on the AVC Visualization Wall." width="200">';?>
          </a>
          <p>The Advanced Visualization center developed a one-of-a-kind <a href="https://www.microsoft.com/en-us/hololens">HoloLens</a> application in collaboration with <a href="http://www.ryancarney.com/">Dr. Ryan Carney</a> that allows the user to see a virtual reconstruction of a specimen of <i lang="la">Archaeopteryx</i> in augmented reality. The AVC created the app with features that allow the user to see the 3D specimen both as it was found in the fossil, and also as an animated complete skeleton. Interactive modes also allow the user to control the wings of <i lang="la">Archaeopteryx</i>. This project received widespread attention through Dr. Ryan Carney both by Fox News and National Geographic.</p>
          <section>
            <h4>Related Media</h4>
            <ul class="bullet-list">
              <li><a href="http://www.fox13news.com/news/local-news/260895703-story">Fox News</a></li>
              <li><a href="http://voices.nationalgeographic.com/2017/06/05/national-geographic-emerging-explorer-ryan-carney-using-x-rays-and-alligators-to-bring-dinosaurs-back-to-life/">National Geographic</a></li>
            </ul>
          </section>
        </article>

        <article>
          <h3 id="cop">Adrenergic Pharmacology VR</h3>
          <a href="showcase/media/projects/cop.png" data-featherlight="image">
            <?php echo '<img class="aside-self" src="'.htmlspecialchars(phpThumbURL('src=../../showcase/media/projects/cop.png&w=200&zc=1', 'libraries/phpThumb/phpThumb.php')).'" alt="Dr. Carney demonstrates his Archeopteryx project on the AVC Visualization Wall." width="200">';?>
          </a>
          <p>The Virtual Reality Adrenergic Pharmacology module is a groundbreaking collaboration between the Advanced Visualization Center and USF College of Pharmacy. Working with Dr. Daniel Lee, the AVC created a virtual reality experience for the Oculus Go that allowed students to learn about adrenergic pharmacology visually and immersively.</p>
          <section>
            <h4>Related Media</h4>
            <ul class="bullet-list">
              <li><a href="https://hscweb3.hsc.usf.edu/blog/2018/10/10/pharmacy-students-use-ar-vr-to-study-impact-of-prescription-drugs-on-our-main-organs/">USF Health Article</a></li>
            </ul>
          </section>
        </article>
      </section>
    </div>
  </main>

  <?php include "../template/footer.php"?>

  <script src="libraries\jquery-3.3.1.min.js"></script>
  <script src="libraries/featherlight/release/featherlight.min.js"></script>
  <link rel="stylesheet" href="libraries/featherlight/release/featherlight.min.css">
  <script src="scripts/featherlight_config.js"></script>
</body>
</html>
