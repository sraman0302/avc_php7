      <nav>          
        <ul>
          <li><a href="http://www.usf.edu/about-usf/index.aspx" target="_blank">About USF</a></li>
          <li><a href="http://www.usf.edu/academics/index.aspx" target="_blank">Academics</a></li>
          <li><a href="http://www.usf.edu/admissions/index.aspx" target="_blank">Admissions</a></li>
          <li><a href="http://www.usf.edu/campus-life/index.aspx" target="_blank">Campus Life</a></li>
          <li><a href="http://www.usf.edu/research/index.aspx" target="_blank">Research</a></li>
          <li><a href="http://www.usf.edu/system/index.aspx" target="_blank">USF System</a></li>
        </ul>
        <ul>
          <li><a href="http://www.usf.edu/partner" target="_blank">Partner with USF</a></li>
          <li><a href="http://www.usf.edu/about-usf/administrative-units.aspx" target="_blank">Administrative Units</a></li>
          <li><a href="http://regulationspolicies.usf.edu/regulations-and-policies/regulations-policies-procedures.asp" target="_blank">Regulations &amp; Policies</a></li>
          <li><a href="http://www.usf.edu/hr/index.aspx" target="_blank">Human Resources</a></li>
          <li><a href="http://www.usf.edu/work-at-usf/index.aspx" target="_blank">Work at USF</a></li>
          <li><a href="http://www.usf.edu/administrative-services/emergency-management/index.aspx" target="_blank">Emergency &amp; Safety</a></li>
        </ul>
        <ul>
          <li><a href="http://health.usf.edu/" target="_blank">USF Health</a></li>
          <li><a href="http://www.gousfbulls.com/" target="_blank">USF Athletics</a></li>
          <li><a href="http://usfalumni.org/" target="_blank">USF Alumni</a></li>
          <li><a href="http://www.unstoppable.usf.edu/" target="_blank">Support USF</a></li>
          <li><a href="http://www.lib.usf.edu/" target="_blank">USF Libraries</a></li>
          <li><a href="http://www.usf.edu/world/index.aspx" target="_blank">USF World</a></li>
        </ul>
      </nav>
      