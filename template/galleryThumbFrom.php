<?php
  # Creates a dynamically loaded image with a featherlight link for viewing
  function galleryThumbFrom($linkUrl, $altText, $imageUrl) {
    $lowResThumbnail = htmlspecialchars(phpThumbURL("src=$imageUrl&w=50&h=50&q=20&zc=1", 'libraries/phpThumb/phpThumb.php'));
    $highResThumbnail = htmlspecialchars(phpThumbURL("src=$imageUrl&w=150&h=150&q=75&zc=1", 'libraries/phpThumb/phpThumb.php'));
    echo "<a href=\"$linkUrl\">
            <img alt=\"$altText\" src=\"$lowResThumbnail\" data-hd-src=\"$highResThumbnail\" class=\"responsive-resolution\">
          </a>";
  }
?>
