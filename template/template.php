<!DOCTYPE html>
<html lang="en">
<!-- Use this code as a template for new pages. Replace comments with content. -->
<head>
  <?php include "template/head.php";?>

  <title><!-- Page title --> | USF AVC</title>

  <meta name="og:title" content="USF Advanced Visualization Center - Title/Headline - Maximum 70 Characters">
  <meta name="og:description" content="Concise description, not title, page-specific. Maximum 200 characters, 2-4 sentences.">
  <meta name="description" content="OK to copy og:description. 60-70 characters.">
  <!-- Replace image with more specific one when applicable to page -->
  <meta name="og:image" content="showcase/media/visualization_wall/dino_model_wall.jpg">

  <meta name="og:image:alt" content="Dinosaur skull displayed on the Advanced Visualization Wall at USF.">
  <meta name="og:url" content="Current page url."> 
  
  <!-- Page-specific scripts and styles -->
</head>
<body itemscope itemtype="http://schema.org/Organization">
  <?php include "template/header.php";?>
  <?php include "template/menu.php";?>

  <main id="main-content">
  <header class="page-header max-width">
      <h1><!-- Page heading--></h1>
    </header>
    <article class="default-body max-width">
      <!-- Page content -->
    </article>
  </main>

  <?php include "template/footer.php"?>
</body>
</html>
