  <footer class="max-width">
    <div id="site-footer">
      <div id="site-info">
        <a href="http://usf.edu">
          <img src="template/footer-logo.png" alt="University of South Florida Logo">
        </a>
        <div>
          <address>
            <div>Copyright © 2018, University of South Florida Advanced Visualization Center. All rights reserved.</div>
            <div><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" id="schema-avc-address"><span itemprop="streetAddress">12010 USF Cherry Drive, CMC147</span>, <span itemprop="addressLocality">Tampa</span>, <span itemprop="addressRegion">FL</span> <span itemprop="postalCode">33620</span>, <span itemProp="addressCountry">USA</span></span></div>
            <div>This website is maintained by the <a href="about/">Advanced Visualization Center (AVC)</a>.</div>
          </address>
          <nav>
              <a href="about/">Contact AVC</a>
              <a href="http://www.usf.edu/about-usf/contact-usf.aspx" target="_blank">Contact USF</a>
              <a href="http://www.usf.edu/about-usf/visit-usf.aspx" target="_blank">Visit USF</a>
              <a href="https://github.com/usf-avc/public-website/issues" target="_blank">Report Site Issues</a>
          </nav>
        </div>    
      </div>
    </div>
  </footer>

  <script src="libraries/cssvars.ponyfill.js"></script>
  <script src="scripts/global.js"></script>
  