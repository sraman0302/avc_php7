# /template

This folder contains all template files used on the site. Most of these files are included in several or all pages. Also included in this folder are any CSS files or images used across all pages.

Note that `base.php` must be updated as described in the main readme for links to function properly.