 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="theme-color" content="#10805e">
  <meta name="geo.region" content="US-FL">
  <meta name="geo.placename" content="Tampa">
  <meta name="og:locale" content="en_US">
  <meta name="og:site_name" content="USF Advanced Visualization Center">
  <meta name="twitter:card" content="summary_large_image">

    <base href="http://127.0.0.1:8080/edsa-AVC%201/">

  
  
  <link rel="stylesheet" href="template/resets.css">
  <link rel="stylesheet" href="template/template.css">
  <link rel="stylesheet" href="template/widgets.css">
  <link rel="shortcut icon" type="image/ico" href="template/favicon.ico"/>
  <?php include 'base.php'?>
  <base href="<?php echo $root?>">

  
  <?php
    # If in root folder, relative url is different
    if(file_exists("libraries/phpThumb/phpThumb.config.php"))
      include "libraries/phpThumb/phpThumb.config.php";
    else
      include "../libraries/phpThumb/phpThumb.config.php";
  ?>

  <link rel="stylesheet" href="template/resets.css">
  <link rel="stylesheet" href="template/template.css">
  <link rel="stylesheet" href="template/widgets.css">
  <link rel="shortcut icon" type="image/ico" href="template/favicon.ico"/>
