<nav id="site-menu">
  <div class="max-width">
    <h2>Navigation</h2>
    <a href="/">Home</a>
    <a href="schedule.php">Schedule</a>
    <a href="resources/index.php">Resources</a>
    <a href="showcase/index.php">Showcase</a>
    <a href="resources/3d_printing.php">3D Printing</a>
    <a href="about/index.php">About Us</a>
  </div>
</nav>
