<?php
if (!function_exists('generateAnchorLink')){
    function generateAnchorLink($anchor) {
  $currentURL = "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
  $escaped = htmlspecialchars($currentURL, ENT_QUOTES, 'UTF-8');
  return $escaped . '#' . $anchor;
}}
?>
<a class="skip-to-main" href="<?php echo generateAnchorLink('main-content') ?>">Skip to main content</a> 

<header>
  <div class="max-width" id="global-header" itemprop="parentOrganization" itemscope itemtype="http://schema.org/EducationalOrganization">
    <a id="global-logo" href="http://usf.edu" itemprop="url"><span itemprop="name">University of South Florida</span></a>
    <nav>
      <a href="http://my.usf.edu">MyUSF</a>
      <a href="http://directory.usf.edu/">Directory</a>
      <a href="http://www.usf.edu/it/">Information Technology</a>
    </nav>
  </div>
  <div id="site-header">
    <div class="max-width">
      <h1>
        <a href="/" itemprop="url"><span itemprop="name">Advanced Visualization Center</span></a>
      </h1>
      <h2 itemprop="parentOrganization" itemscope itemtype="http://schema.org/Organization">
        A Division of <a href="http://www.usf.edu/it/research-computing/" itemprop="url"><span itemprop="name">Research Computing</span></a>
      </h2>
    </div>
  </div>
</header>
